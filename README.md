# TAdv #
WORK IN PROGRESS
A fully featured Text Adventure Game Framework written in C# for Windows Console.

## Features ##

* Complex map creation
* Integration with [Charpix](https://bitbucket.org/lbrocklehurst/charpix) drawing system for drawing objects to Console window
* XP/Level system with assignable Stat points
* Integrated Event system for callbacks on World Objects
* Battling/Combat system
* Basic Enemy pathfinding
* Integration with [TreeSpeech](https://bitbucket.org/lbrocklehurst/treespeech) for NPC dialogue trees
* Customisable 'window' system
* Item morphing functionality for changing or breakable Objects
* Crafting system

## How to Use ##
TODO