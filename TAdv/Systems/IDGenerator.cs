﻿namespace TAdv.Systems
{
    class IDGenerator
    {
        public static string GenerateID()
        {
            return System.Guid.NewGuid().ToString("D");
        }
    }
}
