﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TAdv.WorldElements;

namespace TAdv.Systems
{
    [KnownTypeAttribute(typeof(Item))]
    [KnownTypeAttribute(typeof(Key))]
    [KnownTypeAttribute(typeof(Building))]
    [KnownTypeAttribute(typeof(BasicStatConsumable))]
    [KnownTypeAttribute(typeof(Enemy))]
    [KnownTypeAttribute(typeof(Door))]
    [KnownTypeAttribute(typeof(Landmark))]
    [KnownTypeAttribute(typeof(Weapon))]
    [KnownTypeAttribute(typeof(Armor))]
    [KnownTypeAttribute(typeof(NPC))]
    [KnownTypeAttribute(typeof(Region))]
    [KnownTypeAttribute(typeof(ObjectReference))]
    [DataContract]
    public class Game
    {
        [DataMember]
        public World World;
        [DataMember]
        public PlayableCharacter Player;
        [DataMember]
        private List<Recipe> Recipes;   // Used for saving
        [DataMember]
        public bool Completed = false;

        public enum Action
        {
            help,
            move,
            use,
            examine,
            pick,
            pickup,
            open,
            take,
            talk,
            drop,
            inventory,
            equip,
            combine,
            stats,
            save,
            load
        }

        public Game()
        {
            World = new World();
            Player = new PlayableCharacter("Player", "Player description");
        }

        public Game(World world, PlayableCharacter player)
        {
            World = world;
            Player = player;
        }

        public Game(string location)
        {
            Load(location);
        }

        /// <summary>
        /// Load the Game
        /// </summary>
        /// <param name="location">Location of save game including filename</param>
        public void Load(string location)
        {
            FileInfo locInfo = new FileInfo(location);
            Debug.WriteLine(string.Format("Loading... Path: {0}, Filename: {1}", locInfo.Directory.FullName, locInfo.Name));
            Game loadedGame = Serializer.Deserialize<Game>(locInfo.Directory.FullName, locInfo.Name);
            if (loadedGame == null)
            {
                Debug.WriteLine(string.Format("[FAILED LOADING!] Path: {0}, Filename: {1}", locInfo.Directory.FullName, locInfo.Name));
                return;
            }
            // Sink Data
            World = loadedGame.World;
            Player = loadedGame.Player;
            RecipeDictionary.Recipes = loadedGame.Recipes;
        }
        /// <summary>
        /// Save the Game
        /// </summary>
        /// <param name="location">Location of save game including filename</param>
        public bool Save(string location)
        {
            // Collect Data
            Recipes = RecipeDictionary.Recipes;

            FileInfo locInfo = new FileInfo(new Uri(location).LocalPath);
            Debug.WriteLine(string.Format("Saving... Path: {0}, Filename: {1}", locInfo.Directory.FullName, locInfo.Name));
            return Serializer.Serialize<Game>(this, locInfo.Directory.FullName, locInfo.Name);
        }
    }
}
