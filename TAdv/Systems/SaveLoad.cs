﻿using System.Diagnostics;
using System.IO;

namespace TAdv.Systems
{
    class SaveLoad
    {
        public static string CurrentFile = "";
        /// <summary>
        /// Open up Game file from location
        /// </summary>
        public static Game Open(string location)
        {
            Debug.WriteLine("Opening '" + location + "'...");
            Game game = new Game(location);
            if (game != null)
            {
                CurrentFile = location;
                return game;
            }
            Debug.WriteLine("Failed opening '" + location + "'...");
            return null;
        }
        /// <summary>
        /// Save Game file
        /// </summary>
        public static Game Save(Game game)
        {
            Debug.WriteLine("Saving Game...");

            if (CurrentFile == "")
                return null; // No current file, can't save.

            if (!game.Save(CurrentFile))
            {
                Debug.WriteLine(string.Format("Failed to save Game! {0} doesn't exist!", Path.GetFileName(CurrentFile)));
                return null;
            }
            return game;
        }
        /// <summary>
        /// Save As Game file
        /// </summary>
        public static Game SaveAs(Game game, string location)
        {
            Debug.WriteLine("Saving Game...");
            if (!game.Save(location))
            {
                Debug.WriteLine(string.Format("Failed to save Game! {0} doesn't exist!", Path.GetFileName(CurrentFile)));
                return null;
            }
            return game;
        }
    }
}
