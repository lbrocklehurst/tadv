﻿using Charpix;
using Charpix.ASCIITools;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using TAdv.Drawing;
using TAdv.Physics;
using TAdv.Screens;
using TAdv.Statistics;
using TAdv.WorldElements;

namespace TAdv.Systems
{
    public class Gameplay
    {
        Game Game;

        bool quit = false;
        bool usedMovedAction = false;

        // LAYOUT
        int mapwidth, mapheight,
            titleheight, descriptionheight, playerstatsheight,
            statusheight, help1height, help2height,
            tophalfheight;
        private static string s_statusText = "";
        SurroundingObjects ObjectsAroundPlayer = new SurroundingObjects();
        /// <summary>
        /// Create instance of Gameplay
        /// </summary>
        public Gameplay(Game game)
        {
            Game = game;
        }
        /// <param name="width">Minimum: 60</param>
        /// <param name="height">Minimum: 50</param>
        public Gameplay(Game game, int width, int height)
        {
            if (width < 60)
                width = 60;
            if (height < 50)
                height = 50;
            Game = game;
            Globals.ConsoleWidth = width;
            Globals.ConsoleHeight = height;
        }
        /// <summary>
        /// Build the game ready for starting
        /// </summary>
        public void Build()
        {
            try // Set desired Console size
            {
                Console.SetWindowSize(Globals.ConsoleWidth, Globals.ConsoleHeight);
                Console.SetBufferSize(Globals.ConsoleWidth, Globals.ConsoleHeight);
            }
            catch (ArgumentOutOfRangeException)
            {
                // Let Console size itself
            }
            // Set Colors
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            ASCII.GetImagesInDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ASCII"));
        }
        // Start Game
        public void Start()
        {
            // Make sure the game quits if the user hits ^C
            // Set Console.TreatControlCAsInput to true if you want to use ^C as a valid input value
            Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);

            //Console.CursorVisible = false;

            MainLoop();
        }

        /// <summary>
        /// Event handler for ^C key press
        /// </summary>
        void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Debug.WriteLine("{0} hit, quitting...", e.SpecialKey);
            quit = true;
            e.Cancel = true; // Set this to true to keep the process from quitting immediately
        }

        /// <summary>
        /// The Main Gameloop
        /// </summary>
        void MainLoop()
        {
            const int INTERVAL = 10;
            while (!quit && !Game.Player.Dead)
            {
                mapwidth = Game.World.CurrentRegion.Map.Width;
                mapheight = Game.World.CurrentRegion.Map.Height;
                Thread.Sleep(INTERVAL); // Sleep for a short period
                Draw.Box(0, 0, Globals.ConsoleWidth, Globals.ConsoleHeight, false); // Wipe screen

                Simulate();

                DrawMap();
                DrawTitleAndDescription();
                DrawHelp();
                CalculateTopHeight();
                DrawStatus();
                DrawPlayerStats();
                DrawAvailableActions();
                Menu();
            }
            Draw.PromptBox((Globals.ConsoleWidth - 40) / 2, (Globals.ConsoleHeight - 6) / 2, 40, 6, "Game over!", 1, true, Draw.Alignment.Center, Draw.PromptText.AnyKey, new ColorInfo(), new ColorInfo(ConsoleColor.DarkRed));
            Environment.Exit(0);
        }
        /// <summary>
        /// Draw the Map
        /// </summary>
        void DrawMap()
        {
            Draw.TextBox(0, 0, mapwidth + 2, mapheight + 2, Game.World.CurrentRegion.DrawMap(), 0, true, Draw.Alignment.Center, new ColorInfo(ConsoleColor.Black, ConsoleColor.DarkGreen), new ColorInfo(ConsoleColor.DarkGray, ConsoleColor.Black));
        }
        /// <summary>
        /// Draw the Title and Description boxes
        /// </summary>
        void DrawTitleAndDescription()
        {
            titleheight = Draw.TextBox(mapwidth + 2, 0, Globals.ConsoleWidth - mapwidth - 2, 0, Game.World.CurrentRegion.Name, 0, true, Draw.Alignment.Center, new ColorInfo(ConsoleColor.DarkYellow, ConsoleColor.Black));
            descriptionheight = Draw.TextBox(mapwidth + 2, titleheight - 1, Globals.ConsoleWidth - mapwidth - 2, 2, Game.World.CurrentRegion.Description, 2, true, Draw.Alignment.TopLeft);
            Draw.Row(mapwidth + 2, titleheight - 1, Globals.ConsoleWidth - mapwidth - 2, "-", Chars.VertLine, Chars.VertLine);
        }
        /// <summary>
        /// Draw Status box
        /// </summary>
        void DrawStatus()
        {
            Coord boxLoc = new Coord(0, tophalfheight);
            statusheight = Draw.TextBox(boxLoc.x, boxLoc.y, Globals.ConsoleWidth, 0, s_statusText, 1, true);
            SetStatus("");
        }
        /// <summary>
        /// Static function for setting Status text
        /// </summary>
        public static void SetStatus(string text)
        {
            s_statusText = text;
        }
        /// <summary>
        /// Draw the Player's stat box
        /// </summary>
        void DrawPlayerStats()
        {
            Coord boxLoc = new Coord(0, tophalfheight + statusheight + 4);
            playerstatsheight = Draw.TextBox(boxLoc.x, boxLoc.y, Globals.ConsoleWidth, 0, string.Format("{0}[{1}] │ Health: {2} │ XP: {3} {4}/{5}",
                Game.Player.Name, Game.Player.Level,
                StringTools.GetProgressBar(Game.Player.Health, Game.Player.MaxHealth, 12),
                StringTools.GetProgressBar(Game.Player.XP, Game.Player.MaxXP, 12),
                Game.Player.XP.ToString(), Game.Player.MaxXP.ToString()),
                0, true, Draw.Alignment.Center);
        }
        /// <summary>
        /// Main Input box
        /// </summary>
        void Menu()
        {
            Coord boxLoc = new Coord(0, tophalfheight + statusheight);
            string action = Draw.InputBox(boxLoc.x, boxLoc.y, Globals.ConsoleWidth, 4, "What do you want to do?", 0, true, Draw.Alignment.Center, new ColorInfo(), new ColorInfo(ConsoleColor.DarkYellow));
            if (!PerformAction(action))
                SetStatus(string.Format("'{0}' was not understood.", action));
        }
        /// <summary>
        /// Draw Help section
        /// </summary>
        void DrawHelp()
        {
            int half = (Globals.ConsoleWidth - mapwidth - 2) / 2;
            Coord boxLoc = new Coord(mapwidth + 2, titleheight + descriptionheight - 1);
            string mapkeys = "";
            foreach (MapKey mapkey in Game.World.CurrentRegion.Keys)
                mapkeys += string.Format("{0} - {1}\n", mapkey.Key, mapkey.Name);
            help1height = Draw.TextBox(boxLoc.x, boxLoc.y, half, mapheight + 3 - (titleheight + descriptionheight), mapkeys, 0, false, Draw.Alignment.TopLeft, new ColorInfo(ConsoleColor.DarkGray, ConsoleColor.Black));

            string directiondisplay = string.Format("OBJECTS AROUND:\n{0} \nN: {1}\nE: {2}\nS: {3}\nW: {4}",
                (ObjectsAroundPlayer.Get(WorldSpace.Direction.None).Object != null) ? ObjectsAroundPlayer.Get(WorldSpace.Direction.None).Object.Name : "",
                (ObjectsAroundPlayer.Get(WorldSpace.Direction.North).Object != null) ? ObjectsAroundPlayer.Get(WorldSpace.Direction.North).Object.Name : "",
                (ObjectsAroundPlayer.Get(WorldSpace.Direction.East).Object != null) ? ObjectsAroundPlayer.Get(WorldSpace.Direction.East).Object.Name : "",
                (ObjectsAroundPlayer.Get(WorldSpace.Direction.South).Object != null) ? ObjectsAroundPlayer.Get(WorldSpace.Direction.South).Object.Name : "",
                (ObjectsAroundPlayer.Get(WorldSpace.Direction.West).Object != null) ? ObjectsAroundPlayer.Get(WorldSpace.Direction.West).Object.Name : "");
            
            help2height = Draw.TextBox(boxLoc.x + half, boxLoc.y, half, mapheight + 3 - (titleheight + descriptionheight), directiondisplay, 1, true, Draw.Alignment.TopLeft, new ColorInfo(ConsoleColor.Black, ConsoleColor.DarkGray));
        }
        /// <summary>
        /// Draw a list of available actions/commands
        /// </summary>
        void DrawAvailableActions()
        {
            Coord boxLoc = new Coord(0, tophalfheight + statusheight + 4 + playerstatsheight);
            string actions = "AVAILABLE ACTIONS: ";
            foreach (Game.Action action in Enum.GetValues(typeof(Game.Action)))
                actions += string.Format("{0}, ", StringTools.ToTitleCase(action.ToString()));
            actions = actions.TrimEnd(new char[] { ',', ' ' });
            Draw.TextBox(boxLoc.x, boxLoc.y, Globals.ConsoleWidth, 0, actions,
                0, false, Draw.Alignment.Center);
        }
        /// <summary>
        /// Calculate size of top portion of screen to use with other elements
        /// </summary>
        void CalculateTopHeight()
        {
            int overall = titleheight + descriptionheight +
                ((help1height > help2height) ? help1height : help2height) - 1;
            tophalfheight = (mapheight - 1 > overall) ? mapheight - 1 : overall;
        }
        /// <summary>
        /// Check for objects at position of player and around
        /// </summary>
        private void CheckAroundPlayer()
        {
            Coord playerPos = Game.World.CurrentRegion.Map.PlayerPos;
            ObjectsAroundPlayer.Get(WorldSpace.Direction.None).Object = Game.World.CurrentRegion.GetObjectAtPosition(playerPos.x, playerPos.y);
            ObjectsAroundPlayer.Get(WorldSpace.Direction.North).Object = Game.World.CurrentRegion.GetObjectAtPosition(playerPos.x, playerPos.y - 1);
            ObjectsAroundPlayer.Get(WorldSpace.Direction.East).Object = Game.World.CurrentRegion.GetObjectAtPosition(playerPos.x + 1, playerPos.y);
            ObjectsAroundPlayer.Get(WorldSpace.Direction.South).Object = Game.World.CurrentRegion.GetObjectAtPosition(playerPos.x, playerPos.y + 1);
            ObjectsAroundPlayer.Get(WorldSpace.Direction.West).Object = Game.World.CurrentRegion.GetObjectAtPosition(playerPos.x - 1, playerPos.y);
        }
        /// <summary>
        /// Loop through enemies and check if positioned the same place as Player
        /// </summary>
        private void CheckEnemies()
        {
            foreach(Coord enemyPos in Game.World.CurrentRegion.Map.Enemies)
            {
                if (enemyPos.x == Game.World.CurrentRegion.PlayerPosition.x &&
                   enemyPos.y == Game.World.CurrentRegion.PlayerPosition.y) // Enemy occupies same space as Player
                    FightEnemy(Game.World.CurrentRegion.Map.EnemyAtCoord(enemyPos)); // Fight Enemy
            }
            Game.World.CurrentRegion.Map.UpdateEnemies();
        }
        /// <summary>
        /// Show BattleScreen and fight Enemy
        /// </summary>
        /// <param name="enemy"></param>
        private void FightEnemy(Enemy enemy)
        {
            BattleScreen battlescreen = new BattleScreen(Game.Player, enemy);
            battlescreen.Show();
        }
        private void Simulate()
        {
            if (usedMovedAction)
            {
                Game.World.CurrentRegion.Map.MoveEnemies();
                usedMovedAction = false;
            }
            CheckEnemies();
            CheckAroundPlayer();
            Thread.Sleep(10);   // Delay so it displays
        }
        /// <summary>
        /// Talk to an NPC
        /// </summary>
        /// <param name="npc"></param>
        private void TalkToNPC(NPC npc)
        {
            NPCScreen npcscreen = new NPCScreen(Game.Player, npc);
            npcscreen.Show();
        }
        /// <summary>
        /// Perform an action with optional parameters
        /// </summary>
        private bool PerformAction(string action)
        {
            action = action.ToLower();
            string firstword = StringTools.FirstWord(action);
            Game.Action actionValue;
            if (!Enum.TryParse(firstword, out actionValue))
                return false;
            string[] Params = GetParams(action);
            switch (actionValue)
            {
                case Game.Action.move:
                    return Perform_Move(Params);
                case Game.Action.help:
                    return Perform_Help(Params);
                case Game.Action.use:
                    return Perform_Use(Params);
                case Game.Action.examine:
                    return Perform_Examine(Params);
                case Game.Action.pick:
                case Game.Action.pickup:
                case Game.Action.take:
                    return Perform_Pickup(Params);
                case Game.Action.drop:
                    return Perform_Drop(Params);
                case Game.Action.open:
                    return Perform_Open(Params);
                case Game.Action.talk:
                    return Perform_Talk(Params);
                case Game.Action.inventory:
                    return Perform_Inventory(Params);
                case Game.Action.equip:
                    return Perform_Equip(Params);
                case Game.Action.combine:
                    return Perform_Combine(Params);
                case Game.Action.stats:
                    StatsScreen statsscreen = new StatsScreen(Game.Player);
                    return statsscreen.Show();
                case Game.Action.save:
                    SaveLoadScreen savescreen = new SaveLoadScreen(Game, SaveLoadScreen.State.Save);
                    return savescreen.Show();
                case Game.Action.load:
                    SaveLoadScreen loadscreen = new SaveLoadScreen(Game, SaveLoadScreen.State.Load);
                    return loadscreen.Show();
                default:
                    return false;
            }
        }
        /// <summary>
        /// Move Player
        /// </summary>
        private bool Perform_Move(string[] param)
        {
            Nullable<bool> success = false;
            WorldSpace.Direction direction = WorldSpace.Direction.None;
            switch (param[0])
            {
                case "up":
                case "north":
                case "n":
                    direction = WorldSpace.Direction.North;
                    break;
                case "right":
                case "east":
                case "e":
                    direction = WorldSpace.Direction.East;
                    break;
                case "down":
                case "south":
                case "s":
                    direction = WorldSpace.Direction.South;
                    break;
                case "left":
                case "west":
                case "w":
                    direction = WorldSpace.Direction.West;
                    break;
                default:
                    return false;
            }
            
            if (param.Length > 1 && param[1] != null && param[1] != "") // User may have given an amount number
            {
                int moveAmount = 0;
                if(int.TryParse(param[1], out moveAmount))  // Parse parameter (amount)
                    return Perform_AutoMove(direction, moveAmount);    // Perform auto-move
            }
            success = Game.World.CurrentRegion.MovePlayerPosition(direction, 1);
            usedMovedAction = true;
            if (success == false)
            {
                usedMovedAction = false;    // Cancel move
                SetStatus(string.Format("Cannot Move {0}!", param[0]));
            }
            else if (success == null)    // Player tried to move off screen
            {
                Region border = Game.World.GetRegionFromID(Game.World.CurrentRegion.GetBorderRegionID(direction));
                if (border == null)
                {
                    usedMovedAction = false;    // Cancel move
                    SetStatus("There is nothing for you that way.");
                }
                else
                {
                    Game.World.CurrentRegion.Map.ClearPlayer();
                    Game.World.SetCurrentRegion(border, border.PredictStartPosition(Game.World.CurrentRegion.PlayerPosition, direction));
                }
            }
            return true;
        }
        /// <summary>
        /// Move the Player x amount of spaces
        /// </summary>
        private bool Perform_AutoMove(WorldSpace.Direction direction, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                bool success = Perform_Move(new string[] { direction.ToString().ToLower() });
                if (!success)
                    return false;
                usedMovedAction = true;
                Simulate();
                DrawMap();
            }
            return true;
        }
        /// <summary>
        /// Use an Item
        /// </summary>
        private bool Perform_Use(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;
            bool usingOn = false;
            WorldObject onObject = null;
            if (param.Length > 2
                && param[1] == "on" && param[2] != "")
            {
                // Inventory
                InventorySlot invon = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[2]);
                WorldObject aroundon = ObjectsAroundPlayer.FindObject(param[2]);
                if (invon == null && aroundon == null)
                {
                    SetStatus(string.Format("I don't see '{0}' around here anywhere...", param[2]));
                    return true;
                }
                usingOn = true;
                onObject = (invon != null) ? invon.Item : aroundon;
            }
            // Inventory
            InventorySlot invitem = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[0]);
            WorldObject arounditem = ObjectsAroundPlayer.FindObject(param[0]);
            WorldObject founditem = null;
            if (invitem == null && arounditem == null)
            {
                SetStatus(string.Format("I don't see '{0}' around here anywhere...", param[0]));
                return true;
            }
            founditem = (invitem != null) ? invitem.Item : arounditem;
            if (usingOn)
                SetStatus(onObject.UseObjectOn(founditem));
            else
                SetStatus(founditem.Use(Game));
            return true;
        }
        /// <summary>
        /// Open an Object or Item
        /// </summary>
        private bool Perform_Open(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;

            InventorySlot invitem = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[0]);
            WorldObject arounditem = ObjectsAroundPlayer.FindObject(param[0]);
            WorldObject founditem = null;
            if (invitem == null && arounditem == null)
            {
                SetStatus(string.Format("I don't see '{0}' around here anywhere...", param[0]));
                return true;
            }
            founditem = (invitem != null) ? invitem.Item : arounditem;
            SetStatus(founditem.Open(Game));
            return true;
        }
        /// <summary>
        /// Combine 2 Objects together
        /// </summary>
        private bool Perform_Combine(string[] param)
        {
            if (param.Length < 3 ||
                param[1] != "with" && param[1] != "and")
                return false;

            InventorySlot item1 = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[0]);
            InventorySlot item2 = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[2]);

            if(item1 == null || item2 == null)
            {
                SetStatus(string.Format("There is no '{0}' in your inventory.", (item1 == null) ? param[0] : param[2]));
                return true;
            }
            // Attempt to combine
            WorldObject output = item1.Item.CombineWith(item2.Item);
            if(output == null ||
                !(output is Item))
            {
                SetStatus(string.Format("Cannot combine {0} and {1}!", item1.Item.Name, item2.Item.Name));
                return true;
            }
            Game.Player.Inventory.Remove(item1.Item, 1);
            Game.Player.Inventory.Remove(item2.Item, 1);
            Game.Player.Inventory.Add((Item)output, 1);
            SetStatus(string.Format("Combined {0} and {1}!\n{2} was added to your inventory!", item1.Item.Name, item2.Item.Name, output.Name));
            return true;
        }
        /// <summary>
        /// Drop an Object
        /// </summary>
        private bool Perform_Drop(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;
            InventorySlot invitem = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[0]);
            if (invitem == null)
            {
                SetStatus(string.Format("There is no '{0}' in your inventory.", param[0]));
                return true;
            }
            Game.Player.Inventory.Remove(invitem.Item, 1);
            Game.World.CurrentRegion.AddItem(invitem.Item, Game.World.CurrentRegion.PlayerPosition.x, Game.World.CurrentRegion.PlayerPosition.y);
            SetStatus(string.Format("You dropped {0}!", invitem.Item.Name));
            return true;
        }
        /// <summary>
        /// Equip an EquipableItem from Inventory
        /// </summary>
        private bool Perform_Equip(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;

            InventorySlot invitem = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[0]);
            if (invitem == null)
            {
                SetStatus(string.Format("There is no '{0}' in your inventory.", param[0]));
                return true;
            }
            if(!(invitem.Item is EquipableItem))
                SetStatus(string.Format("Cannot equip {0}", param[0]));
            else
            {
                EquipableItem swapped = Game.Player.Equip((EquipableItem)invitem.Item);
                if(swapped == null)
                    SetStatus(string.Format("Equipped {0}!", param[0]));
                else
                    SetStatus(string.Format("Equipped {0} and removed {1}!", param[0], swapped));
            }
            return true;
        }
        /// <summary>
        /// Talk to and/or interact with an NPC
        /// </summary>
        private bool Perform_Talk(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;
            int mainParam = 0;
            if (param[0] == "to") // Handle user entering 'talk to'
                mainParam = 1;

            DirectionObject found = ObjectsAroundPlayer.Find(param[mainParam]);
            if (found == null)
            {
                SetStatus(string.Format("Can't talk to '{0}' as it doesn't exist!", param[mainParam]));
                return true;
            }
            if (!(found.Object is NPC))
            {
                SetStatus(string.Format("...I don't think talking to {0} will help.", param[mainParam]));
                return true;
            }
            NPC npc = (NPC)found.Object;
            if (npc.HasDialogue)
                TalkToNPC(npc);
            else
                SetStatus(string.Format("{0}: {1}", npc.Name, npc.Talk()));
            Coord foundposition = CoordInDirection(found.Direction);
            return true;
        }
        /// <summary>
        /// Examine an Object
        /// </summary>
        private bool Perform_Examine(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;
            // Inventory
            InventorySlot found = Game.Player.Inventory.Items.Find(x => x.Item.Name.ToLower() == param[0]);
            if (found != null) // Found specified item in Inventory
            {
                SetStatus(string.Format("{0}: {1}", found.Item.Name, found.Item.Description));
                return true;
            }
            // Around Player
            WorldObject aroundfound = ObjectsAroundPlayer.FindObject(param[0]);
            if (aroundfound == null) // Didn't find object around Player
            {
                SetStatus(string.Format("I don't see '{0}' around here anywhere...", param[0]));
                return true;
            }
            SetStatus(string.Format("{0}: {1}", aroundfound.Name, aroundfound.Description));
            return true;
        }
        /// <summary>
        /// Pick up an Object
        /// </summary>
        private bool Perform_Pickup(string[] param)
        {
            if (param[0] == null || param[0] == "")
                return false;
            int mainParam = 0;
            if (param[0] == "up") // Handle user entering 'pick up'
                mainParam = 1;

            DirectionObject found = ObjectsAroundPlayer.Find(param[mainParam]);
            if(found == null)
            {
                SetStatus(string.Format("There is no '{0}' to pick up!", param[mainParam]));
                return true;
            }
            if (!(found.Object is Item))
            {
                SetStatus(string.Format("Can't pick up {0}!", param[mainParam]));
                return true;
            }
            Coord foundposition = CoordInDirection(found.Direction);
            if (Game.World.CurrentRegion.Map.RemoveObject((Item)found.Object, foundposition.x, foundposition.y))
            {
                Game.Player.Inventory.Add((Item)found.Object, 1);
                SetStatus(string.Format("You picked up {0}!", param[mainParam]));
            }
            else
                SetStatus(string.Format("Can't pick up {0}!", param[mainParam]));
            return true;
        }
        /// <summary>
        /// Have a look at the items in Player's Inventory
        /// </summary>
        private bool Perform_Inventory(string[] param)
        {
            string items = "";
            InventorySlot[] inventory = Game.Player.Inventory.Items.ToArray();
            if(inventory.Length <= 0)
            {
                SetStatus("There's nothing in your Inventory!");
                return true;
            }
            for (int i = 0; i < inventory.Length; i++)
            {
                items += string.Format("{0} ({1})", inventory[i].Item.Name, inventory[i].Amount);
                if (i != inventory.Length - 1)
                    items += ", ";
            }
            items.TrimEnd(',');
            SetStatus(string.Format("Inventory: {0}", items));
            return true;
        }
        /// <summary>
        /// Ask for help on a specific command/action
        /// </summary>
        private bool Perform_Help(string[] param)
        {
            if (param[0] == null || param[0] == "")
            {
                SetStatus("Provides help on each command.\nUsage: help <command>");
                return true;
            }
            // TODO: Have the descriptions and usage taken from an external/formatted list
            Game.Action actionValue;
            if (!Enum.TryParse(param[0], out actionValue))
                return false;
            string description = "", usage = "";
            switch (actionValue)
            {
                case Game.Action.move:
                    description = "Move in a direction.";
                    usage = "move <direction> (eg. north, n, up) <amount> (optional)";
                    break;
                case Game.Action.examine:
                    description = "Get extra information on an object.";
                    usage = "examine <object>";
                    break;
                case Game.Action.use:
                    description = "Use an object / Use an object on another object.";
                    usage = "use <object> / use <object> on <object>";
                    break;
                case Game.Action.equip:
                    description = "Equip an item from your Inventory.";
                    usage = "equip <item>";
                    break;
                case Game.Action.pick:
                case Game.Action.pickup:
                case Game.Action.take:
                    description = "Pick up an item and put into your Inventory.";
                    usage = "pickup/take <item>";
                    break;
                case Game.Action.inventory:
                    description = "Examine the contents of your Inventory.";
                    usage = "inventory";
                    break;
                case Game.Action.drop:
                    description = "Remove an item from your Inventory and place on the ground.";
                    usage = "drop <item>";
                    break;
                case Game.Action.combine:
                    description = "Attempt to combine two items together.";
                    usage = "combine <item> with/and <item>";
                    break;
                case Game.Action.load:
                    description = "Load a saved game.";
                    usage = "load";
                    break;
                case Game.Action.save:
                    description = "Save your game.";
                    usage = "save";
                    break;
                case Game.Action.stats:
                    description = "Enter the Stats screen.";
                    usage = "stats";
                    break;
                case Game.Action.talk:
                    description = "Talk to an NPC.";
                    usage = "talk to <NPC>";
                    break;
                default:
                    return false;
            }
            SetStatus(string.Format("{0}\nUsage: {1}", description, usage));
            return true;
        }
        /// <summary>
        /// Get the Coord from Player in a desired direction
        /// </summary>
        private Coord CoordInDirection(WorldSpace.Direction direction)
        {
            Coord targetPos = new Coord(Game.World.CurrentRegion.Map.PlayerPos);
            switch (direction)
            {
                case WorldSpace.Direction.North:
                    targetPos.y -= 1;
                    break;
                case WorldSpace.Direction.East:
                    targetPos.x += 1;
                    break;
                case WorldSpace.Direction.South:
                    targetPos.y += 1;
                    break;
                case WorldSpace.Direction.West:
                    targetPos.x -= 1;
                    break;
            }
            return targetPos;
        }
        /// <summary>
        /// Split up a string by it's spaces and output to a string[] param array
        /// </summary>
        private static string[] GetParams(string input)
        {
            input = input.Replace(StringTools.FirstWord(input), "");
            input = input.Trim();
            string[] Params = input.Split(' ');
            foreach (string param in Params)
                param.Trim();
            return Params;
        }
    }
}
