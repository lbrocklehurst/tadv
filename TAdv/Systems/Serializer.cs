﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace TAdv.Systems
{
    class Serializer
    {
        public static bool Serialize<T>(T data, string location, string name)
        {
            string fullPath = Path.Combine(location, name);
            try
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter writer = XmlWriter.Create(fullPath, settings))
                    serializer.WriteObject(writer, data);
                return true;
            }
            catch (Exception e)
            {
                Exception ie = e.InnerException;
                if (ie != null)
                    Debug.WriteLine("Exception: " + ie.ToString());
                else
                    Debug.WriteLine("Exception: " + e.ToString());
            }
            return false;
        }

        public static T Deserialize<T>(string location, string name)
        {
            string fullPath = Path.Combine(location, name);
            T serializedData = default(T);
            try
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                using(XmlReader reader = XmlReader.Create(fullPath))
                    serializedData = (T)serializer.ReadObject(reader);
                return serializedData;
            }
            catch (Exception e)
            {
                Exception ie = e.InnerException;
                if (ie != null)
                    Console.WriteLine("Exception w/ Inner: " + ie.ToString());
                else
                    Console.WriteLine("Exception: " + e.ToString());
                return serializedData;
            }
        }
    }
}
