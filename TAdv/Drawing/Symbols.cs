﻿namespace TAdv.Drawing
{
    class Symbols
    {
        public static string Player = "ð", Enemy = "¶", DeadEnemy = "§",
            Landmark = "X", Item = "¤", BrokenObject = "¼", NPC = "¿";
    }
}
