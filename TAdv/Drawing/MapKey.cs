﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAdv.Drawing
{
    [DataContract]
    public class MapKey
    {
        [DataMember]
        public char Key;
        [DataMember]
        public string Name;

        public MapKey(char key, string name)
        {
            Key = key;
            Name = name;
        }
    }
}
