﻿using System.Runtime.Serialization;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class BasicStatConsumable : Consumable
    {
        [DataMember]
        private int ChangeHealth = 0, ChangeXP = 0, ChangeHunger = 0;
        [DataMember]
        private string ConsumeMessage = "";

        public BasicStatConsumable(string name, string description,
            string consumeMessage = "",
            int health = 0, int xp = 0, int hunger = 0)
            : base(name, description)
        {
            ConsumeMessage = consumeMessage;
            ChangeHealth = health;
            ChangeXP = xp;
            ChangeHunger = hunger;
        }
        public override string Use(Game game)
        {
            return Use(game.Player);
        }
        public override string Use(PlayableCharacter player)
        {
            player.Health += ChangeHealth;
            player.AddXP(ChangeXP);
            player.Hunger += ChangeHunger;
            string response = (ConsumeMessage != "") ? ConsumeMessage : string.Format("Used {0}!", this.Name);
            response += (ChangeHealth != 0) ? string.Format("\n{0} HP!", ChangeHealth) : "";
            response += (ChangeXP != 0) ? string.Format("\n{0} XP!", ChangeXP) : "";
            response += (ChangeHunger != 0) ? string.Format("\n{0} Hunger!", ChangeHunger) : "";
            return response;
        }
    }
}
