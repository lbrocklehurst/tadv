﻿using System.Runtime.Serialization;
using TreeSpeech;

namespace TAdv.WorldElements
{
    [DataContract]
    public class NPC : WorldObject
    {
        [DataMember]
        public Dialogue Dialogue;
        [DataMember]
        string Phrase = "";
        [DataMember]
        public string Image = "";

        public bool HasDialogue
        {
            get { return (Dialogue != null); }
        }

        public NPC(string name, string description,
            Dialogue dialoguetree = null, string phrase = "",
            string image = "defaultnpc")
            : base(name, description)
        {
            this.Dialogue = dialoguetree;
            this.Phrase = phrase;
            this.Image = image;
        }

        public virtual string Talk()
        {
            return (HasDialogue) ? Dialogue.Speak() : Phrase;
        }

        public bool HasResponses
        {
            get { return Dialogue.HasResponses; }
        }
    }
}
