﻿using System;
using System.Runtime.Serialization;
using TAdv.Statistics;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Enemy : WorldObject
    {
        [DataMember]
        public AttributePoints Attributes = new AttributePoints();
        [DataMember]
        public int MaxHealth, Level;
        [DataMember]
        private int m_health, m_baseDamage = 2, m_baseDefense = 2;
        [DataMember]
        private float m_strength = 1.4f;
        [DataMember]
        public bool Dead = false;
        [DataMember]
        public string Image = "";

        public int Health
        {
            get { return m_health; }
            set
            {
                m_health = MathHelper.Clamp(value, 0, MaxHealth);
                if (m_health <= 0)
                    Dead = true;
            }
        }
        /// <summary>
        /// Calculated power value
        /// </summary>
        public int Power { get { return m_baseDamage * (int)Math.Round(Math.Pow(Level + 1, m_strength * 0.08f)); } }

        public int Defense
        {
            get { return m_baseDefense * (int)Math.Round(Math.Pow(Level + 1, Attributes.Get(AttributePoint.Attributes.Endurance).Points * 0.1f)); }
        }

        public int XPReward
        {
            get { return Power * 100; } // TODO: Flesh this out
        }

        /// <summary>
        /// Initialises a new instance of WorldElements.Enemy
        /// </summary>
        public Enemy()
        {
            Level = 1;
            MaxHealth = 50;
            Health = MaxHealth;
            m_strength = 1;
        }
        /// <param name="enemyLevel">Enemy's level</param>
        /// <param name="maxhealth">Maximum health value</param>
        /// <param name="strength">Strength value</param>
        /// <param name="asciiimage">ASCIIImage name for UI image</param>
        public Enemy(string name, string description,
            int enemyLevel = 1, int maxhealth = 50, int strength = 1,
            string asciiimage = "default")
            : base(name, description)
        {
            MaxHealth = maxhealth;
            Health = MaxHealth;
            Level = enemyLevel;
            this.Image = asciiimage;
            m_strength = strength;
            GenerateStats(Level);
        }

        private void GenerateStats(int numberOfPoints)
        {
            Random random = new Random();
            int pointsLeft = numberOfPoints;

            while (pointsLeft > 0)
            {
                Attributes.Attributes[random.Next(0, Attributes.Attributes.Count)].Points += 1;
                pointsLeft--;
            }

            //foreach(AttributePoint attribute in Attributes.Attributes)
            //    Debug.WriteLine("{0}'s {1} points = {2}", this.Name, attribute.Attribute.ToString(), attribute.Points);
        }
    }
}
