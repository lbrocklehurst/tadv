﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Key : Item
    {
        [DataMember]
        public string Code;

        public Key(string code = "", string name = "Key", string description = "")
            : base(name, description)
        {
            Code = code;
        }
    }
}
