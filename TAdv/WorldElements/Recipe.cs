﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Recipe
    {
        [DataMember]
        public List<string> Materials = new List<string>();
        [DataMember]
        public WorldObject Output = null;

        public Recipe(WorldObject material1, WorldObject material2, WorldObject output)
        {
            this.Output = output;
            Materials.Add(material1.Name);
            Materials.Add(material2.Name);
        }
        public Recipe(string material1, string material2, WorldObject output)
        {
            this.Output = output;
            Materials.Add(material1);
            Materials.Add(material2);
        }
        public Recipe(string[] materials, WorldObject output)
        {
            this.Output = output;
            Materials.AddRange(materials);
        }
        /// <summary>
        /// Check if the materials match exactly to materials in Recipe
        /// </summary>
        public bool Match(string[] materials)
        {
            if (materials == null ||
                materials.Length != this.Materials.Count)
                return false;
            bool success = true;
            foreach(string name in materials)
            {
                if(this.Materials.Find(x => x == name) == null)
                {
                    success = false;
                    break;
                }
            }
            return success;
        }
    }
}
