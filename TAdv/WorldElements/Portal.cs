﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Portal : WorldObject
    {
        [DataMember]
        public Region Destination;

        public Portal(Region destination)
        {
            Destination = destination;
        }
    }
}
