﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TAdv.Statistics;

namespace TAdv.WorldElements
{
    [DataContract]
    public class PlayableCharacter : WorldObject
    {
        [DataMember]
        public AttributePoints Attributes = new AttributePoints();
        [DataMember]
        public Inventory Inventory = new Inventory();
        [DataMember]
        public PlayerSlots EquipSlots = new PlayerSlots();
        [DataMember]
        private XPLevel XPLevel = new XPLevel();
        [DataMember]
        private int m_health, m_hunger, m_maxHunger = 100, m_baseDamage = 2, m_baseDefense = 2;
        [DataMember]
        public int MaxHealth;
        [DataMember]
        public bool Dead = false;

        public PlayableCharacter(string n, string d, int maxhealth = 100)
            : base(n, d)
        {
            MaxHealth = maxhealth;
            Health = MaxHealth;
        }

        public int Health
        {
            get { return m_health; }
            set
            {
                m_health = MathHelper.Clamp(value, 0, MaxHealth);
                if (m_health <= 0)
                    Dead = true;
            }
        }

        public int Power
        {
            get
            {
                Weapon weapon = GetEquip(EquipableItem.Part.Weapon) as Weapon;
                int weaponpower = (weapon != null) ? weapon.BaseDamage : 1;
                return m_baseDamage * (int)Math.Round(Math.Pow(Level + 1, Attributes.Get(AttributePoint.Attributes.Strength).Points * 0.1f + (weaponpower * 0.1f)));
            }
        }

        public int Defense
        {
            get
            {
                int totalDefense = 0;
                foreach(EquipableItem slotitem in EquipSlots.Slots)
                {
                    if(slotitem is Armor)
                    {
                        Armor armor = (Armor)slotitem;
                        totalDefense += armor.DefenseValue;
                    }
                }
                return m_baseDefense * (int)Math.Round(Math.Pow(Level + 1, Attributes.Get(AttributePoint.Attributes.Endurance).Points * 0.1f + (totalDefense * 0.05f)));
            }
        }

        public int XP { get { return XPLevel.XP; } }
        public int MaxXP { get { return XPLevel.MaxXP; } }
        public int Level { get { return XPLevel.Level; } }

        public int Hunger
        {
            get { return m_hunger; }
            set { m_hunger = MathHelper.Clamp(value, 0, m_maxHunger); }
        }

        public void AddXP(int amount)
        {
            int oldlevel = XPLevel.Level;
            XPLevel.AddXP(amount);
            int difference = XPLevel.Level - oldlevel;
            if (difference != 0) // Has level changed?
                Attributes.SparePoints += difference; // Add attribute points for each level
        }

        public EquipableItem Equip(EquipableItem item)
        {
            EquipableItem found = GetEquip(item.ItemPart);
            if (found != null)
            {
                EquipSlots.Remove(found);
                Inventory.Add(found, 1);
            }
            EquipSlots.Add(item);
            Inventory.Remove(item);
            return found;
        }

        public EquipableItem GetEquip(EquipableItem.Part part)
        {
            return EquipSlots.Slots.Find(x => x.ItemPart == part);
        }
    }
    public class PlayerSlots
    {
        public List<EquipableItem> Slots = new List<EquipableItem>();

        public void Add(EquipableItem item) { Slots.Add(item); }
        public void Remove(EquipableItem item) { Slots.Remove(item); }
    }
}
