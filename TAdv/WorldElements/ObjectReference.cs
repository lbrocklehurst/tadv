﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class ObjectReference : WorldObject
    {
        public ObjectReference(string name)
            : base(name, "")
        {

        }
    }
}
