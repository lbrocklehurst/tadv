﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Item : WorldObject
    {
        public Item(string name, string description)
            : base(name, description)
        {

        }
    }
}
