﻿using System;
using System.Runtime.Serialization;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    public delegate void StateChangedEventHandler(object sender, EventArgs e);
    public delegate void PerformActionEventHandler(object sender, EventArgs e, bool success);

    [DataContract]
    public class WorldObject
    {
        public enum State
        {
            None,
            Morphed,
            Broken
        }
        [DataMember]
        public string Name = "";
        [DataMember]
        public string Description = "";
        private State m_currentState = State.None;
        [DataMember]
        public string UseHint = "", UseOnHint = "", OpenHint = "";

        public event StateChangedEventHandler StateChanged;
        public event PerformActionEventHandler EventUseAction, EventUseOnAction, EventOpenAction, EventCombineAction, EventTalkAction;
        [DataMember]
        public State CurrentState
        {
            get { return m_currentState; }
            set
            {
                m_currentState = value;
                Changed(EventArgs.Empty);
            }
        }

        // Events //
        protected virtual void Changed(EventArgs e) { if (StateChanged != null) StateChanged(this, e); } // State Changed

        protected virtual void UseEvent(EventArgs e, bool success) { if (EventUseAction != null) EventUseAction(this, e, success); }
        protected virtual void UseOnEvent(EventArgs e, bool success) { if (EventUseOnAction != null) EventUseOnAction(this, e, success); }
        protected virtual void OpenEvent(EventArgs e, bool success) { if (EventOpenAction != null) EventOpenAction(this, e, success); }
        protected virtual void CombineEvent(EventArgs e, bool success) { if (EventCombineAction != null) EventCombineAction(this, e, success); }
        protected virtual void TalkEvent(EventArgs e, bool success) { if (EventTalkAction != null) EventTalkAction(this, e, success); }

        // Event Handlers
        public virtual void ReceiveUse(object sender, EventArgs e, bool success) { if (success) Use(null); }
        public virtual void ReceiveOpen(object sender, EventArgs e, bool success) { if (success) Open(null); }

        public WorldObject(string n = "", string d = "")
        {
            Name = n;
            Description = d;
        }

        public virtual string Use(Game game) { UseEvent(EventArgs.Empty, false); return string.Format("Cannot use {0}! {1}", Name, UseHint); }
        public virtual string UseObjectOn(WorldObject otherobject) { UseOnEvent(EventArgs.Empty, false); return string.Format("Cannot use {0} on {1}! {2}", otherobject.Name, Name, UseOnHint); }
        public virtual string Open(Game game) { OpenEvent(EventArgs.Empty, false); return string.Format("Cannot open {0}! {1}", Name, OpenHint); }
        public virtual WorldObject CombineWith(WorldObject otherobject) { CombineEvent(EventArgs.Empty, false); return RecipeDictionary.FindOutput(this.Name, otherobject.Name); }
        public virtual string Talk(Game game) { TalkEvent(EventArgs.Empty, false); return string.Format("Can't talk to {0}!", Name); }
    }
}
