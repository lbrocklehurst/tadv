﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Landmark : WorldObject
    {
        public Landmark(string name, string description)
            : base(name, description)
        {

        }
    }
}
