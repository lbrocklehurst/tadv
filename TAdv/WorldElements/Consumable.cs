﻿using System.Runtime.Serialization;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Consumable : Item
    {
        public Consumable(string name, string description)
            : base(name, description)
        {
        }

        public override string Use(Game game)
        {
            return string.Format("{0} didn't do anything!", this.Name);
        }
        public virtual string Use(PlayableCharacter player)
        {
            return string.Format("{0} didn't do anything!", this.Name);
        }
    }
}
