﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Weapon : EquipableItem
    {
        [DataMember]
        public int BaseDamage = 2;

        public Weapon(string name, string description, int baseDamage = 2)
            : base(name, description, Part.Hands)
        {
            this.BaseDamage = baseDamage;
            ItemPart = Part.Weapon;
        }
    }
}
