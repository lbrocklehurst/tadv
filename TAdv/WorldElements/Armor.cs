﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Armor : EquipableItem
    {
        [DataMember]
        public int DefenseValue = 2;

        public Armor(string name, string description, Part part, int defenseValue = 2)
            : base(name, description, part)
        {
            this.DefenseValue = defenseValue;
            ItemPart = part;
        }
    }
}
