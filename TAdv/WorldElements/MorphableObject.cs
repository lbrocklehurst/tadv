﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class MorphableObject : WorldObject
    {
        [DataMember]
        private WorldObject MorphInto = null;
        public string MorphIntoSymbol = "";

        public virtual void ReceiveMorph(object sender, EventArgs e, bool success) { if (success) CurrentState = State.Morphed; }

        public MorphableObject(string name, string description, WorldObject morph, string morphsymbol = "X")
            : base(name, description)
        {
            MorphInto = morph;
            MorphIntoSymbol = morphsymbol;
        }

        public virtual WorldObject Morph()
        {
            Debug.WriteLine(string.Format("[{0}] It's morphin' time!", this.Name));
            if (MorphInto == null)
                return null;
            return MorphInto;
        }
    }
}
