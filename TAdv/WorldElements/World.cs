﻿using Charpix;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class World : WorldObject
    {
        [DataMember]
        public List<Region> Regions = new List<Region>();
        [DataMember]
        private int m_currentRegionNo = 0;

        public void CreateRegion(Region region)
        {
            // Generate a unique ID for this region if one does not exist - or the ID already exists within List
            if (region.ReferenceID == "" || RegionExistsWithID(region.ReferenceID))
            {
                region.ReferenceID = IDGenerator.GenerateID();
                CreateRegion(region);   // Attempt to add again (in case Unique ID not unique)
                return;
            }
            Debug.WriteLine(string.Format("Adding region '{0}' with ID: {1}", region.Name, region.ReferenceID));
            Regions.Add(region);
        }

        public void SetCurrentRegion(Region region, Coord playerpos = null)
        {
            int index = Regions.FindIndex(x => x == region);
            m_currentRegionNo = index;
            if (playerpos == null)
                playerpos = CurrentRegion.StartPosition;
            CurrentRegion.SetPlayerPosition(playerpos.x, playerpos.y);
        }
        public void SetCurrentRegion(string regionID, Coord playerpos = null)
        {
            int index = Regions.FindIndex(x => x.ReferenceID == regionID);
            m_currentRegionNo = index;
            if (playerpos == null)
                playerpos = CurrentRegion.StartPosition;
            CurrentRegion.SetPlayerPosition(playerpos.x, playerpos.y);
        }

        public Region GetRegionFromID(string id)
        {
            return Regions.Find(x => x.ReferenceID == id);
        }

        public Region CurrentRegion { get { return Regions[m_currentRegionNo]; } }

        public bool RegionExistsWithID(string ID) { return (GetRegionFromID(ID) != null); }
    }
}
