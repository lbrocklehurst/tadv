﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class TriggerObject : WorldObject
    {
        public TriggerObject(string name, string description)
            : base(name, description)
        {

        }

        public override string Use(Game game)
        {
            UseEvent(EventArgs.Empty, true);
            return string.Format("You used {0}!", Name);
        }
    }
}
