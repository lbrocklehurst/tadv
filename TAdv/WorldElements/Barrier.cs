﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Barrier : MorphableObject
    {
        public Barrier(string name, string description, WorldObject morph, string morphsymbol = "X")
            : base(name, description, morph, morphsymbol)
        {

        }
    }
}
