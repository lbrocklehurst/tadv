﻿using System.Collections.Generic;

namespace TAdv.WorldElements
{
    public class RecipeDictionary
    {
        public static List<Recipe> Recipes = new List<Recipe>();

        public static void Add(Recipe recipe)
        {
            Recipes.Add(recipe);
        }

        public static WorldObject FindOutput(string material1, string material2)
        {
            string[] materials = new string[]{ material1, material2 };
            return FindOutput(materials);
        }

        public static WorldObject FindOutput(string[] materials)
        {
            foreach (Recipe recipe in Recipes)
                if (recipe.Match(materials))
                    return recipe.Output;
            return null;
        }

        public static string[] GetRecipe(WorldObject theObject)
        {
            Recipe recipe = Recipes.Find(x => x.Output.Name == theObject.Name);
            return (recipe != null) ? recipe.Materials.ToArray() : null;
        }
    }
}
