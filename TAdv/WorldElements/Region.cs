﻿using Charpix;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TAdv.Drawing;
using TAdv.Physics;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Region : WorldObject
    {
        [DataMember]
        public string ReferenceID = "";
        [DataMember]
        public WorldSpace Map = new WorldSpace();
        [DataMember]
        public SurroundingObjects BorderingRegions = new SurroundingObjects();
        [DataMember]
        public Coord StartPosition;
        [DataMember]
        public List<MapKey> Keys = new List<MapKey>();

        public Region(int w, int l, string n = "", string d = "", Coord startPos = null, string referenceID = "")
            : base(n, d)
        {
            if(startPos == null) startPos = new Coord();
            if (referenceID == "") ReferenceID = IDGenerator.GenerateID(); 
            StartPosition = startPos;
            Map = new WorldSpace(w, l);
            AddKey(Symbols.Player[0], "You");
        }
        //--------//
        // PLAYER //
        //--------//
        /// <summary>
        /// Move Player's position
        /// </summary>
        public Nullable<bool> MovePlayerPosition(WorldSpace.Direction direction, int amount)
        {
            return Map.MovePlayer(direction, amount);
        }
        /// <summary>
        /// Set Player's position
        /// </summary>
        public bool SetPlayerPosition(int x, int y)
        {
            return Map.SetPlayerPosition(x, y);
        }
        public Coord PlayerPosition { get { return Map.PlayerPos; } }
        /// <summary>
        /// Get Player's current position
        /// </summary>
        public Coord CurrentPosition
        {
            get { return Map.PlayerPos; }
            set { Map.PlayerPos = value; }
        }
        /// <summary>
        /// Predict Start position based on which side the Player entered
        /// </summary>
        /// <param name="oldPosition">Position in last region</param>
        /// <param name="enterDirection">Direction the player is facing when entering (or side player exited through)</param>
        public Coord PredictStartPosition(Coord oldPosition, WorldSpace.Direction enterDirection)
        {
            Coord position = oldPosition;

            switch(WorldSpace.OppositeDirection(enterDirection)) // The side of which the player entered
            {
                case WorldSpace.Direction.North:
                    position.y = 0;
                    break;
                case WorldSpace.Direction.East:
                    position.x = Map.Width - 1;
                    break;
                case WorldSpace.Direction.South:
                    position.y = Map.Height - 1;
                    break;
                case WorldSpace.Direction.West:
                    position.x = 0;
                    break;
            }
            // Clamp values
            position.x = MathHelper.Clamp(position.x, 0, Map.Width - 1);
            position.y = MathHelper.Clamp(position.y, 0, Map.Height - 1);

            return position;
        }
        /// <summary>
        /// Add map key to key list
        /// </summary>
        void AddKey(char key, string name)
        {
            if (Keys.Find(x => x.Key == key) != null)
                return;
            Keys.Add(new MapKey(key, name));
        }
        /// <summary>
        /// Get the WorldObject at Player's current position
        /// </summary>
        public WorldObject GetObjectAtPosition()
        {
            return Map.Space[CurrentPosition.x][CurrentPosition.y].TopObject;
        }
        /// <summary>
        /// Get the WorldObject at (x, y)
        /// </summary>
        public WorldObject GetObjectAtPosition(int x, int y)
        {
            if (x >= Map.Width || y >= Map.Height
                || x < 0 || y < 0)
                return null;
            return Map.Space[x][y].TopObject;
        }
        /// <summary>
        /// Add a Landmark
        /// </summary>
        /// <param name="d">Custom Symbol</param>
        public void AddLandmark(Landmark landmark, int x, int y, string d = "")
        {
            string symbol = (d != "") ? d : Symbols.Landmark;
            Map.PlaceAtCoord(x, y, symbol, landmark);
            AddKey(symbol[0], "Landmark");
        }
        /// <summary>
        /// Place an Enemy on the map
        /// </summary>
        public void AddEnemy(Enemy enemy, int x, int y)
        {
            Map.AddEnemy(enemy, x, y);
            AddKey(Symbols.Enemy[0], "Enemy");
        }
        /// <summary>
        /// Add a bordering region to this region
        /// </summary>
        public void AddBorderRegion(Region region, WorldSpace.Direction direction)
        {
            BorderingRegions.Get(direction).Object = new ObjectReference(region.ReferenceID);
            WorldSpace.Direction opposite = WorldSpace.OppositeDirection(direction);
            if(region.GetBorderRegionID(opposite) == null) region.AddBorderRegion(this, opposite);
        }
        /// <summary>
        /// Get the bordering region from desired direction
        /// </summary>
        public string GetBorderRegionID(WorldSpace.Direction direction)
        {
            return (BorderingRegions.Get(direction).Object is ObjectReference) ? BorderingRegions.Get(direction).Object.Name : null;
        }
        /// <summary>
        /// Place an item onto the map
        /// </summary>
        /// <param name="d">Optional display symbol</param>
        public void AddItem(Item item, int x, int y, string d = "")
        {
            Map.AddObject(item, x, y, d);
            AddKey((d == "") ? Symbols.Item[0] : d[0], "Item");
        }
        /// <summary>
        /// Place an object onto the map
        /// </summary>
        /// <param name="d">Display symbol</param>
        public void AddObject(WorldObject worldobj, int x, int y, string d, bool addToKeys = false)
        {
            Map.AddObject(worldobj, x, y, d);
            if(addToKeys)
                AddKey(d[0], worldobj.Name);
        }
        /// <summary>
        /// Place an NPC onto the map
        /// </summary>
        /// <param name="d">Optional display symbol</param>
        public void AddNPC(NPC npc, int x, int y, string d = "", bool addToKeys = true)
        {
            string symbol = (d != "") ? d : Symbols.NPC;
            Map.AddObject(npc, x, y, symbol);
            if(addToKeys)
                AddKey(symbol[0], "NPC");
        }
        /// <summary>
        /// Add a building structure
        /// </summary>
        /// <param name="direction">Direction the building (and door) is facing</param>
        public void AddBuilding(Building building, int x, int y, int width, int height, Door door = null, WorldSpace.Direction direction = WorldSpace.Direction.North)
        {            
            for(int h = 0; h < height; h++)
            {
                if(h == 0)
                    AddRow(x, y + h, width, "─", "┌", "┐", building);
                else if(h == height - 1)
                    AddRow(x, y + h, width, "─", "└", "┘", building);
                else
                    AddRow(x, y + h, width, " ", "│", "│", building);
            }
            if (door == null)
                return;
            Coord doorPos = new Coord();
            string symbol = " ";
            switch(direction)
            {
                case WorldSpace.Direction.North:
                    doorPos.x = x + (width / 2);
                    doorPos.y = y;
                    symbol = Chars.HorizLine;
                    break;
                case WorldSpace.Direction.East:
                    doorPos.x = x + width - 1;
                    doorPos.y = y + (height / 2);
                    symbol = Chars.VertLine;
                    break;
                case WorldSpace.Direction.South:
                    doorPos.x = x + (width / 2);
                    doorPos.y = y + height - 1;
                    symbol = Chars.HorizLine;
                    break;
                case WorldSpace.Direction.West:
                    doorPos.x = x;
                    doorPos.y = y + (height / 2);
                    symbol = Chars.VertLine;
                    break;
            }
            Map.PlaceAtCoord(doorPos.x, doorPos.y, symbol, door);
        }
        /// <summary>
        /// Add row of objects to map
        /// </summary>
        /// <param name="symbol">Main symbol</param>
        /// <param name="start">Start symbol</param>
        /// <param name="end">End symbol</param>
        private void AddRow(int x, int y, int length, string symbol, string start = "", string end = "", WorldObject worldobj = null)
        {
            for (int i = 0; i < length; i++)
            {
                if (i == 0)
                    Map.PlaceAtCoord(x + i, y, (start != "") ? start : symbol, worldobj);
                else if (i == length - 1)
                    Map.PlaceAtCoord(x + i, y, (end != "") ? end : symbol, worldobj);
                else
                    Map.PlaceAtCoord(x + i, y, symbol, worldobj);
            }
        }
        /// <summary>
        /// Add line of objects to map
        /// </summary>
        /// <param name="symbol">Main symbol</param>
        /// <param name="start">Start symbol</param>
        /// <param name="end">End symbol</param>
        public void AddLine(int x, int y, int x2, int y2, string symbol, string start = "", string end = "")
        {
            int w = x2 - x;
            int h = y2 - y;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                if (i == 0)
                    Map.PlaceAtCoord(x, y, (start != "") ? start : symbol);
                else if (i == longest)
                    Map.PlaceAtCoord(x, y, (end != "") ? end : symbol);
                else
                    Map.PlaceAtCoord(x, y, symbol);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x += dx1;
                    y += dy1;
                }
                else
                {
                    x += dx2;
                    y += dy2;
                }
            }
        }
        /// <summary>
        /// Add circle of symbols to the Map
        /// </summary>
        /// <param name="outline">Outline symbol</param>
        public void AddCircle(int centerX, int centerY, int radius, string symbol, string outline = "", bool fill = true)
        {
            double dDensity = 1.0;
            double y, x;
            for (x = -radius; x <= radius; x += dDensity)
            {
                y = Math.Sqrt(-Math.Pow(x, 2.0f) + Math.Pow(radius, 2.0f));
                if (fill)
                    AddLine((int)x + centerX, (int)y + centerY, (int)x + centerX, centerY, symbol, (outline != "") ? outline : symbol, symbol);
                else
                    Map.PlaceAtCoord((int)x + centerX, (int)y + centerY, symbol);
                y = -y;
                if (fill)
                    AddLine((int)x + centerX, (int)y + centerY, (int)x + centerX, centerY, symbol, (outline != "") ? outline : symbol, symbol);
                else
                    Map.PlaceAtCoord((int)x + centerX, (int)y + centerY, symbol);
            }
        }
        /// <summary>
        /// Draw map of Region
        /// </summary>
        public string DrawMap()
        {
            return Map.Draw();
        }
    }
}
