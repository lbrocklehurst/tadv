﻿using Charpix;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using TAdv.Drawing;
using TAdv.Physics;

namespace TAdv.WorldElements
{
    [DataContract]
    public class WorldSpace
    {
        [DataMember]
        public WorldPoint[][] Space;
        [DataMember]
        public Coord PlayerPos = new Coord();
        [DataMember]
        public List<Coord> Enemies = new List<Coord>();
        public enum Direction
        {
            North,
            East,
            South,
            West,
            None
        }
        /// <summary>
        /// Create a new instance of the WorldSpace class
        /// </summary>
        public WorldSpace() { }
        /// <param name="w"></param>
        /// <param name="l"></param>
        public WorldSpace(int w, int l)
        {
            Space = new WorldPoint[w][]; // Create and fill coords with Coord classes
            for (int X = 0; X < w; X++)
            {
                Space[X] = new WorldPoint[l];
                for (int Y = 0; Y < l; Y++)
                {
                    Space[X][Y] = new WorldPoint();
                }
            }
        }
        /// <summary>
        /// Get Width of WorldSpace
        /// </summary>
        public int Width { get { return Space.Length; } }
        /// <summary>
        /// Get Height of WorldSpace
        /// </summary>
        public int Height { get { return (Space.Length > 0) ? Space[0].Length : 0; } }

        private void ObjectChanged(object sender, EventArgs e)
        {
            WorldObject theObject = (WorldObject)sender;
            Debug.WriteLine(theObject.Name + "'s state changed.");
            //if(theObject.CurrentState == WorldObject.State.Destroyed)

        }
        /// <summary>
        /// Get the opposite direction
        /// </summary>
        public static Direction OppositeDirection(Direction direction)
        {
            switch(direction)
            {
                case Direction.North:
                    return Direction.South;
                case Direction.South:
                    return Direction.North;
                case Direction.East:
                    return Direction.West;
                case Direction.West:
                    return Direction.East;
            }
            return Direction.None;
        }

        /// <summary>
        /// Replaces everything and places a WorldObject at x, y in the WorldSpace
        /// </summary>
        /// <param name="d">Display icon (single character)</param>
        /// <param name="wo">WorldObject to place</param>
        public void PlaceAtCoord(int x, int y, string d, WorldObject wo = null)
        {
            if (ExtendsBounds(x, y))
            {
                Debug.WriteLine(string.Format("[WorldSpace.cs] PlaceAtCoord(): Specified coords '{0}, {1}' are out of bounds!", x, y));
                return;
            }
            Space[x][y] = new WorldPoint(d, wo);
        }
        /// <summary>
        /// Add an Enemy to this position in WorldSpace
        /// </summary>
        public bool AddEnemy(Enemy enemy, int x, int y)
        {
            if (!CheckWorldPosition(x, y))
                return false;
            Enemies.Add(new Coord(x, y));
            Space[x][y].AddLayer(Symbols.Enemy, enemy);
            return true;
        }
        /// <summary>
        /// Add an Object to this position in WorldSpace
        /// </summary>
        /// <param name="symbol">Display icon (single character)</param>
        public bool AddObject(WorldObject item, int x, int y, string symbol = "")
        {
            if (!CheckWorldPosition(x, y))
                return false;
            Space[x][y].AddLayer((symbol == "") ? Symbols.Item : symbol, item);
            return true;
        }
        /// <summary>
        /// Remove a specific WorldObject from this position in WorldSpace
        /// </summary>
        public bool RemoveObject(WorldObject item, int x, int y)
        {
            if (Space[x][y].RemoveLayerByObjectName(item.Name))
                return true;
            return false;
        }
        /// <summary>
        /// Move all of the enemies
        /// </summary>
        public void MoveEnemies()
        {
            for(int e = 0; e < Enemies.Count; e++) // Move each Enemy
            {
                if (EnemyAtCoord(Enemies[e]) != null && EnemyAtCoord(Enemies[e]).Dead)
                    continue;
                Coord newPos = PredictEnemyMovement(Enemies[e]); // Get new position of enemy
                Enemies[e] = SetEnemyPosition(Enemies[e], newPos.x, newPos.y); // Set position
            }
        } 
        public void UpdateEnemies()
        {
            for (int e = 0; e < Enemies.Count; e++)
            {
                Enemy enemy;
                enemy = EnemyAtCoord(Enemies[e]);
                if (enemy == null)
                {
                    Space[Enemies[e].x][Enemies[e].y].RemoveLayer(Symbols.Enemy);
                    Enemies.Remove(Enemies[e]);
                    continue;
                }
                if(enemy.Dead)
                {
                    if (Space[Enemies[e].x][Enemies[e].y].RemoveLayer(Symbols.Enemy))
                        Space[Enemies[e].x][Enemies[e].y].AddLayer(Symbols.DeadEnemy);
                }
            }
        }
        /// <summary>
        /// Set position of Enemy
        /// </summary>
        Coord SetEnemyPosition(Coord oldPos, int x, int y)
        {
            if (!CheckWorldPosition(x, y, new Type[] { typeof(Structure), typeof(Enemy) }))
                return oldPos;
            Enemy enemy;
            if ((enemy = EnemyAtCoord(oldPos)) == null)
                return oldPos;
            Space[oldPos.x][oldPos.y].RemoveLayer(Symbols.Enemy);
            Space[x][y].AddLayer(Symbols.Enemy, enemy);
            return new Coord(x, y);
        }
        /// <summary>
        /// Move the player
        /// </summary>
        public Nullable<bool> MovePlayer(Direction direction, int amount)
        {
            int amountX = 0, amountY = 0;
            switch(direction)
            {
                case Direction.North:
                    amountY = -1;
                    break;
                case Direction.East:
                    amountX = 1;
                    break;
                case Direction.South:
                    amountY = 1;
                    break;
                case Direction.West:
                    amountX = -1;
                    break;
            }
            //int newX = MathHelper.Clamp(PlayerPos.x + amountX, 0, Width - 1);
            //int newY = MathHelper.Clamp(PlayerPos.y + amountY, 0, Height - 1);
            int newX = PlayerPos.x + amountX;
            int newY = PlayerPos.y + amountY;

            if (!ExtendsBounds(newX, newY))
                return SetPlayerPosition(newX, newY);

            return null;
        }
        /// <summary>
        /// Set new position of Player
        /// </summary>
        public bool SetPlayerPosition(int x, int y)
        {
            if (!CheckWorldPosition(x, y))
                return false;
            Space[PlayerPos.x][PlayerPos.y].RemoveLayer(Symbols.Player);
            PlayerPos.x = x;
            PlayerPos.y = y;
            Space[PlayerPos.x][PlayerPos.y].AddLayer(Symbols.Player);
            return true;
        }
        public void ClearPlayer()
        {
            Space[PlayerPos.x][PlayerPos.y].RemoveLayer(Symbols.Player);
        }
        public Enemy EnemyAtCoord(Coord position)
        {
            return (Enemy)Space[position.x][position.y].GetLayer(Symbols.Enemy);
        }
        private Coord PredictEnemyMovement(Coord enemyPos)
        {
            Raycast raycast = new Raycast();
            if (PerformRaycast(enemyPos, PlayerPos, new Type[] { typeof(Structure), typeof(Barrier) }, out raycast))
                return enemyPos; // Can't see the player, do not move
            if (raycast.Line.Length > 1) // Can see the player, move 1 in direction
                return raycast.Line[1];
            else
                return enemyPos;
        }
        private bool PerformRaycast(Coord start, Coord end, Type[] LayerMask, out Raycast ray)
        {
            ray = new Raycast();
            ray.Line = DrawTools.Line(start.x, start.y, end.x, end.y);
            ray.Point = start;
            bool hit = false;
            for (int i = 0; i < ray.Line.Length; i++)
            {
                WorldObject baseobj = Space[ray.Line[i].x][ray.Line[i].y].BaseObject;
                Type found = (baseobj == null) ? null : Array.Find(LayerMask, x => x.IsAssignableFrom(baseobj.GetType()));
                if (baseobj != null && found != null)
                {
                    hit = true;
                    break;
                }
                ray.Point = ray.Line[i];
            }
            return hit;
        }
        /// <summary>
        /// Check if two object take up the same World Space
        /// </summary>
        private bool ObjectsIntersectAtPoint(int x, int y, WorldObject object1, WorldObject object2)
        {
            WorldPointInfo found1 = Space[x][y].Layers.Find(o1 => o1.WorldObject == object1);
            WorldPointInfo found2 = Space[x][y].Layers.Find(o2 => o2.WorldObject == object2);
            if (found1 != null && found2 != null)
                return true;
            return false;
        }
        /// <summary>
        /// Check if coords extend map bounds
        /// </summary>
        private bool ExtendsBounds(int x, int y)
        {
            return (x >= Width || x < 0 || y >= Height || y < 0);
        }
        /// <summary>
        /// Check if object can be moved to this position
        /// </summary>
        private bool CheckWorldPosition(int x, int y, Type[] LayerMask = null)
        {
            if (ExtendsBounds(x, y))
                return false;

            if (LayerMask == null)
                LayerMask = new Type[] { typeof(Structure), typeof(Barrier) };

            foreach (WorldPointInfo info in Space[x][y].Layers)
            {
                Type found = (info.WorldObject == null) ? null : Array.Find(LayerMask, o => o.IsAssignableFrom(info.WorldObject.GetType()));
                if (info.WorldObject != null && found != null)
                    return false;
            }
            return true;
        }
        /// <summary>
        /// Draw out WorldSpace to Console
        /// </summary>
        public string Draw()
        {
            string map = "";
            for (int Y = 0; Y < Height; Y++)
            {
                map += "\n";
                for (int X = 0; X < Width; X++)
                {
                    map += Space[X][Y].Display;
                }
            }
            return map;
        }
    }
    public class WorldPoint
    {
        public List<WorldPointInfo> Layers = new List<WorldPointInfo>();
        public WorldPoint()
        {
            AddLayer("░", null);
        }
        public WorldPoint(string d = "░", WorldObject wo = null)
        {
            AddLayer(d, wo); // Create base layer
        }

        // Event Handler for change in WorldObject
        private void ObjectChanged(object sender, EventArgs e)
        {
            WorldObject theObject = (WorldObject)sender;
            Debug.WriteLine(theObject.Name + "'s state changed.");
            switch(theObject.CurrentState)
            {
                case WorldObject.State.Morphed:
                    WorldObject newObject = null;
                    if (theObject is MorphableObject) // Check if object is a Morphable object and change to new object if so
                    {
                        newObject = (theObject as MorphableObject).Morph();
                        RemoveLayerByObjectName(theObject.Name);
                        AddLayer((theObject as MorphableObject).MorphIntoSymbol, newObject);
                    }
                    break;
                case WorldObject.State.Broken:
                    WorldObject brokenObject = null;
                    if (theObject is MorphableObject) // Check if object is a Morphable object and change to new object if so
                    {
                        newObject = (theObject as MorphableObject).Morph();
                        RemoveLayerByObjectName(theObject.Name);
                        AddLayer(Symbols.BrokenObject, brokenObject);
                    }
                    break;
                default:
                    break;
            }
        }

        public string Display { get { return Layers[Layers.Count - 1].Display; } }              // Top layer display
        public WorldObject TopObject
        {
            get
            {
                if (Layers.Count == 0)
                    return null;
                return (Layers[Layers.Count - 1].Display == Symbols.Player) ? Layers[Layers.Count - 2].WorldObject : Layers[Layers.Count - 1].WorldObject;
            }
        }
        public WorldObject BaseObject { get { return Layers[0].WorldObject; } }                 // Base (Bottom) Object

        public void AddLayer(string d, WorldObject wo = null)
        {
            Layers.Add(new WorldPointInfo(d, wo));
            if (wo != null)
                wo.StateChanged += new StateChangedEventHandler(ObjectChanged);
        }

        public bool RemoveLayer(string symbol)
        {
            if (symbol == "")
                return false;
            WorldPointInfo found = Layers.Find(x => x.Display == symbol);
            if (found == null)
                return false;
            Layers.Remove(found);
            return true;
        }
        public bool RemoveLayerByObjectName(string name)
        {
            if (name == "")
                return false;
            WorldPointInfo found = null;
            foreach (WorldPointInfo info in Layers)
                if (info.WorldObject != null && info.WorldObject.Name == name)
                    found = info;
            if (found == null)
                return false;
            Layers.Remove(found);
            return true;
        }
        public WorldObject GetLayer(string symbol)
        {
            if (symbol == "")
                return null;
            WorldPointInfo found = Layers.Find(x => x.Display == symbol);
            if (found == null)
                return null;
            return found.WorldObject;
        }
    }
    public class WorldPointInfo
    {
        public WorldObject WorldObject;
        public string Display;

        public WorldPointInfo()
        {
            WorldObject = null;
            Display = "░";
        }
        public WorldPointInfo(string d = "░", WorldObject wo = null)
        {
            WorldObject = wo;
            Display = d;
        }
    }
}
