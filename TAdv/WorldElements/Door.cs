﻿using Charpix;
using System;
using System.Runtime.Serialization;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class Door : Portal
    {
        [DataMember]
        public bool Locked;
        [DataMember]
        public string UnlockCode;

        public Door(Region destination = null, bool locked = false, string unlockcode = "")
            : base(destination)
        {
            Locked = locked;
            UnlockCode = unlockcode;
            Name = "Door";
        }

        public Region Open()
        {
            return (Locked) ? null : Destination;
        }

        public bool Unlock(Key key)
        {
            if (!Locked) // Already unlocked!
                return true;
            if (key.Code == UnlockCode) // Key's 'code' matches the door's
            {
                Locked = false;
                return true;
            }
            return false;
        }

        public override string Use(Game game)
        {
            if (!Locked)
            {
                game.World.SetCurrentRegion(Destination, new Coord());
                UseEvent(EventArgs.Empty, true);
                return string.Format("You entered {0}!", Destination.Name);
            }
            else
            {
                UseEvent(EventArgs.Empty, false);
                return string.Format("{0} is locked!", Name);
            }
        }

        public override string UseObjectOn(WorldObject otherobject)
        {
            if(otherobject is Key)
            {
                Key key = (Key)otherobject;
                if (Locked)
                    if (Unlock(key))
                    {
                        UseOnEvent(EventArgs.Empty, true);
                        return string.Format("{0} unlocked!", Name);
                    }
                    else
                    {
                        UseOnEvent(EventArgs.Empty, false);
                        return string.Format("{0} doesn't work on {1}...", otherobject.Name, Name);
                    }
                else
                {
                    UseOnEvent(EventArgs.Empty, true);
                    return string.Format("{0} is already unlocked!", Name);
                }
            }
            return base.UseObjectOn(otherobject);
        }

        public override string Open(Game game)
        {
            if (!Locked)
            {
                game.World.SetCurrentRegion(Destination, new Coord());
                OpenEvent(EventArgs.Empty, true);
                return string.Format("You entered {0}!", Destination.Name);
            }
            else
            {
                OpenEvent(EventArgs.Empty, false);
                return string.Format("{0} is locked!", Name);
            }
        }
    }
}
