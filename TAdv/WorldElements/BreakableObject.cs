﻿using System;
using System.Runtime.Serialization;
using TAdv.Systems;

namespace TAdv.WorldElements
{
    [DataContract]
    public class BreakableObject : MorphableObject
    {
        [DataMember]
        WorldObject requiredObject; // Object required to break this

        public BreakableObject(string name, string description, WorldObject morph, WorldObject requiredobject = null)
            : base(name, description, morph)
        {
            this.requiredObject = requiredobject;
        }
        /// <summary>
        /// Check if object can be broke or not, then change as necessary
        /// </summary>
        /// <param name="usedtobreak">Object being used on this object, if any</param>
        bool Break(WorldObject usedtobreak = null)
        {
            if (requiredObject != null)
            {
                if (WorldObjectHelper.Compare(requiredObject, usedtobreak)) // Does the used object match the required one?
                {
                    CurrentState = State.Broken;
                    return true;
                }
                return false;
            }
            CurrentState = State.Broken;
            return true;
        }

        public override string Use(Game game)
        {
            bool broke = Break();
            UseEvent(EventArgs.Empty, broke);
            return (broke) ? string.Format("{0} is now broken!", Name) : string.Format("Cannot use {0}!", Name);
        }

        public override string UseObjectOn(WorldObject otherobject)
        {
            bool broke = Break(otherobject);
            UseOnEvent(EventArgs.Empty, broke);
            return (broke) ? string.Format("Used {0} on {1}! {1} is now broken!", otherobject.Name, Name) : string.Format("{0} did nothing to {1}.", otherobject.Name, Name);
        }
    }
}
