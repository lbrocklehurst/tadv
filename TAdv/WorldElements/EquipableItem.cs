﻿using System.Runtime.Serialization;

namespace TAdv.WorldElements
{
    [DataContract]
    public class EquipableItem : Item
    {
        public enum Part
        {
            Head,
            Hands,
            Chest,
            Legs,
            Feet,
            Weapon
        }
        [DataMember]
        public Part ItemPart;

        public EquipableItem(string name, string description, Part part)
            : base(name, description)
        {
            ItemPart = part;
        }
    }
}
