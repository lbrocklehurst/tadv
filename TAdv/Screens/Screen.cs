﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAdv.Screens
{
    class Screen
    {
        /// <summary>
        /// Show Screen
        /// </summary>
        public virtual bool Show()
        {
            MainLoop();
            Console.ReadLine();
            return true;
        }
        /// <summary>
        /// Screen's main loop
        /// </summary>
        public virtual void MainLoop()
        {
            //
        }
    }
}
