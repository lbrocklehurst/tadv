﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.WorldElements;
using TAdv.Drawing;
using TAdv.Statistics;
using TAdv.Systems;
using Charpix;

namespace TAdv.Screens
{
    class StatsScreen : MenuScreen
    {
        public new enum State
        {
            Main,
            AddAttribute,
            RemoveAttribute
        }
        public new State CurrentState = State.Main;

        PlayableCharacter Player;

        int column1width = 35,
                column2width = 0,
                toptitleheight = 0,
                generalstatsheight = 0,
                equipmentheight = 0,
                attributesheight = 0;

        public StatsScreen(PlayableCharacter player)
        {
            this.Player = player;
        }

        public override void MainLoop()
        {
            column2width = Globals.ConsoleWidth - column1width;

            DrawTitle();
            DrawGeneral();
            DrawEquipment();
            DrawAttributes();
            statusPosition.y =
            inputPosition.y = toptitleheight + attributesheight;
        }

        public override void Menu()
        {
            menucommands = "Select option ('exit' to Exit):\n";

            switch(CurrentState)
            {
                case State.Main:
                    Menu_Main();
                    break;
                case State.AddAttribute:
                    Menu_AddAttribute();
                    break;
                case State.RemoveAttribute:
                    Menu_RemoveAttribute();
                    break;
            }
        }

        public override void Menu_Main()
        {
            menucommands += "1 - Add Attribute Point\n2 - Remove Attribute Point";
            MenuInput();
            switch(input)
            {
                case "1":
                    CurrentState = State.AddAttribute;
                    break;
                case "2":
                    CurrentState = State.RemoveAttribute;
                    break;
                default:
                    status = string.Format("Didn't understand '{0}'", input);
                    break;
            }
        }

        void Menu_AddAttribute()
        {
            menucommands += "[Add Attribute Point]\n";
            for (int i = 0; i < Player.Attributes.Attributes.Count; i++)
            {
                AttributePoint point = Player.Attributes.Attributes[i];
                menucommands += string.Format("{0} - {1}\n", i + 1, point.Attribute.ToString());
                if(i == Player.Attributes.Attributes.Count - 1)
                    menucommands += string.Format("{0} - Go back...", i + 2);
            }
            MenuInput();
            int menunumber = 0;
            if (!int.TryParse(input, out menunumber))
            {
                status = string.Format("Didn't understand '{0}'", input);
                return;
            }
            if(menunumber == Player.Attributes.Attributes.Count + 1)
            {
                CurrentState = State.Main;
                return;
            }
            AttributePoint.Attributes attr = (AttributePoint.Attributes)menunumber - 1;
            if (Player.Attributes.AssignPoints(attr, 1))
                status = string.Format("Added a point to {0}!", attr.ToString());
            else
                status = string.Format("Cannot add any points to {0}!", attr.ToString());
        }

        void Menu_RemoveAttribute()
        {
            menucommands += "[Remove Attribute Point]\n";
            for (int i = 0; i < Player.Attributes.Attributes.Count; i++)
            {
                AttributePoint point = Player.Attributes.Attributes[i];
                menucommands += string.Format("{0} - {1}\n", i + 1, point.Attribute.ToString());
                if (i == Player.Attributes.Attributes.Count - 1)
                    menucommands += string.Format("{0} - Go back...", i + 2);
            }
            MenuInput();
            int menunumber = 0;
            if (!int.TryParse(input, out menunumber))
                return;
            if (menunumber == Player.Attributes.Attributes.Count + 1)
            {
                CurrentState = State.Main;
                return;
            }
            AttributePoint.Attributes attr = (AttributePoint.Attributes)menunumber - 1;
            if (Player.Attributes.AssignPoints(attr, -1))
                status = string.Format("Removed point from {0}!", attr.ToString());
            else
                status = string.Format("Cannot remove any points from {0}!", attr.ToString());
        }

        void DrawTitle()
        {
            toptitleheight = Draw.TextBox(0, 0, Globals.ConsoleWidth, 1, string.Format("{0} [Level {1}]", Player.Name, Player.Level), 0);
        }

        void DrawAttributes()
        {
            string attributepoints = "";
            for (int i = 0; i < Player.Attributes.Attributes.Count; i++)
            {
                AttributePoint point = Player.Attributes.Attributes[i];
                attributepoints += string.Format("{0} {1} ({2}/{3})\n", point.Attribute.ToString(), StringTools.GetProgressBar(point.Points, point.Max, 10), point.Points.ToString(), point.Max.ToString());
            }
            attributepoints += "\n \nSpare Points: " + Player.Attributes.SparePoints;
            attributesheight = Draw.TextBox(column1width, toptitleheight, column2width, generalstatsheight + equipmentheight, attributepoints, 1, true, Draw.Alignment.Right, new ColorInfo(), new ColorInfo(ConsoleColor.Red));
        }

        void DrawGeneral()
        {
            int titlewidth = 12, infowidth = column1width - titlewidth;
            string generalStats_Titles = "Health:\n \nXP:";
            string generalStats_Info = StringTools.GetProgressBar(Player.Health, Player.MaxHealth, 10) + "\n \n";
            generalStats_Info += string.Format("{0} {1}/{2}\n", StringTools.GetProgressBar(Player.XP, Player.MaxXP, 10), Player.XP, Player.MaxXP);
            generalstatsheight = Draw.TextBox(titlewidth - 1, toptitleheight, infowidth + 1, 2, generalStats_Info, 1, true, Draw.Alignment.Left);
            Draw.TextBox(0, toptitleheight, titlewidth, generalstatsheight, generalStats_Titles, 1, true, Draw.Alignment.Right);
            Draw.Line(titlewidth - 1, toptitleheight, titlewidth - 1, toptitleheight + generalstatsheight - 1, "│", Chars.HorizLine, Chars.HorizLine);
        }

        void DrawEquipment()
        {
            string equipment = "\nEquipment:\n";
            foreach (EquipableItem.Part part in Enum.GetValues(typeof(EquipableItem.Part)))
            {
                EquipableItem item = Player.GetEquip(part);
                equipment += string.Format("\n{0}: {1}", part.ToString(), (item == null) ? " - " : item.Name);
            }
            equipmentheight = Draw.TextBox(0, toptitleheight + generalstatsheight, column1width, 2, equipment, 1, true, Draw.Alignment.Left);
        }
    }
}
