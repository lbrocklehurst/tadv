﻿using Charpix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.Drawing;
using TAdv.Statistics;

namespace TAdv.Screens
{
    class MenuScreen : Screen
    {
        public enum State
        {
            Main
        }
        public State CurrentState = State.Main;

        public int statusHeight = 0;
        public string input = "", menucommands = "", status = "";
        public Coord inputPosition = new Coord(),
            statusPosition = new Coord();

        /// <summary>
        /// Show Screen
        /// </summary>
        public override bool Show()
        {
            while (input != "exit")
            {
                Draw.Box(0, 0, Globals.ConsoleWidth, Globals.ConsoleHeight, false); // Wipe screen

                MainLoop();
                DrawStatus();
                Menu();
            }
            return true;
        }
        /// <summary>
        /// Draw Menu
        /// </summary>
        public virtual void Menu()
        {
            menucommands = "Select option ('exit' to Exit):\n";

            switch (CurrentState)
            {
                case State.Main:
                    Menu_Main();
                    break;
            }
        }
        /// <summary>
        /// Main Menu
        /// </summary>
        public virtual void Menu_Main()
        {
            menucommands += "Main Menu";
            MenuInput();
            switch (input)
            {
                default:
                    status = string.Format("Didn't understand '{0}'", input);
                    break;
            }
        }
        /// <summary>
        /// Draw Status box
        /// </summary>
        public virtual void DrawStatus()
        {
            statusHeight = Draw.TextBox(statusPosition.x, statusPosition.y, Globals.ConsoleWidth, 0, status, 1, true);
            status = "";
        }
        /// <summary>
        /// Draw Input box
        /// </summary>
        public virtual void MenuInput()
        {
            input = Draw.InputBox(inputPosition.x, inputPosition.y + statusHeight, Globals.ConsoleWidth, 4,
                menucommands, 0, true, Draw.Alignment.Left, new ColorInfo(), new ColorInfo(ConsoleColor.DarkYellow));
        }

        public void ExitScreen() { input = "exit"; }
    }
}
