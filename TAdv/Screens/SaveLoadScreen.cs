﻿using Charpix;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TAdv.Statistics;
using TAdv.Systems;

namespace TAdv.Screens
{
    class SaveLoadScreen : MenuScreen
    {
        public new enum State
        {
            Main,
            Save,
            Load
        }
        public new State CurrentState = State.Main;

        Game game;
        List<Game> SaveSlots = new List<Game>();

        int toptitleheight = 0, saveslotsheight = 0;

        public SaveLoadScreen(Game game)
        {
            this.game = game;
        }
        public SaveLoadScreen(Game game, State state)
        {
            this.game = game;
            this.CurrentState = state;
        }

        public override void MainLoop()
        {
            DrawTitle();
            DrawSaveSlots();
            statusPosition.y = inputPosition.y = toptitleheight + saveslotsheight;
        }

        public override void Menu()
        {
            menucommands = "Select option ('exit' to Exit):\n";

            switch (CurrentState)
            {
                case State.Main:
                    Menu_Main();
                    break;
                case State.Save:
                    Menu_Save();
                    break;
                case State.Load:
                    Menu_Load();
                    break;
            }
        }

        public override void Menu_Main()
        {
            menucommands += "1 - Save Game\n2 - Load Game";
            MenuInput();
            switch (input)
            {
                case "1":
                    CurrentState = State.Save;
                    break;
                case "2":
                    CurrentState = State.Load;
                    break;
                default:
                    status = string.Format("Didn't understand '{0}'", input);
                    break;
            }
        }

        void Menu_Save()
        {
            menucommands += "SELECT A SAVE SLOT";
            MenuInput();

            int menunumber = 0;
            if (!int.TryParse(input, out menunumber)
                || menunumber > Globals.SaveSlotNumber
                || menunumber <= 0)
            {
                status = string.Format("Didn't understand '{0}'", input);
                return;
            }
            // Save into slot <menunumber - 1>
            menucommands = "Input Save Name:";
            MenuInput();
            string SavesPath = Path.Combine(Globals.AppDirectory, "Saves");
            string FullPath = Path.Combine(SavesPath, input + Globals.SaveFiletype);
            SaveLoad.SaveAs(game, FullPath);
        }

        void Menu_Load()
        {
            menucommands += "SELECT A LOAD SLOT";
            MenuInput();

            int menunumber = 0;
            if (!int.TryParse(input, out menunumber)
                || menunumber > Globals.SaveSlotNumber
                || menunumber <= 0)
            {
                status = string.Format("Didn't understand '{0}'", input);
                return;
            }
            if (menunumber > SaveSlots.Count) // Empty slot
            {
                status = string.Format("Save slot '{0}' is empty.", menunumber);
                return;
            }
            // Load from slot <menunumber - 1>
            Game chosen = SaveSlots[menunumber - 1];
            game.World = chosen.World;
            game.Player = chosen.Player;
            game.Completed = chosen.Completed;
            ExitScreen();
        }

        void DrawTitle()
        {
            toptitleheight = Draw.TextBox(0, 0, Globals.ConsoleWidth, 1, "Save/Load Game", 0);
        }

        void DrawSaveSlots()
        {
            SaveSlots.Clear();

            Coord boxPos = new Coord(0, toptitleheight);
            string SavesPath = Path.Combine(Globals.AppDirectory, "Saves");
            Uri SavesUri = new Uri(SavesPath);
            Directory.CreateDirectory(SavesUri.LocalPath);
            string[] filePaths = Directory.GetFiles(SavesUri.LocalPath, string.Format("*{0}", Globals.SaveFiletype));

            string savesDisplay = "";

            for(int i = 0; i < Globals.SaveSlotNumber; i++)
            {
                Game loadedGame = null;
                if (i >= filePaths.Length || filePaths[i] == null
                    || (loadedGame = new Game(filePaths[i])).World == null   // Attempt to load
                    )
                {
                    savesDisplay += string.Format("\n{0} - {1}", i + 1, "EMPTY");
                    continue;
                }
                SaveSlots.Add(loadedGame);
                savesDisplay += string.Format("\n{0} - {1} [{2}|Level {3}]", i + 1, Path.GetFileNameWithoutExtension(filePaths[i]), loadedGame.Player.Name, loadedGame.Player.Level);
            }
            saveslotsheight = Draw.TextBox(boxPos.x, boxPos.y, Globals.ConsoleWidth, 1, savesDisplay, 1);
        }
    }
}
