﻿using Charpix;
using Charpix.ASCIITools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.Drawing;
using TAdv.Statistics;
using TAdv.WorldElements;

namespace TAdv.Screens
{
    class BattleScreen : MenuScreen
    {
        public new enum State
        {
            Main,
            UseItem
        }
        public new State CurrentState = State.Main;

        PlayableCharacter Player;
        Enemy Enemy;
        int imageheight,
            playerstatboxheight, enemystatboxheight,
            statboxwidth;

        List<string> itemList = new List<string>();

        public BattleScreen(PlayableCharacter player, Enemy enemy)
        {
            this.Player = player;
            this.Enemy = enemy;
        }

        public override void MainLoop()
        {
            statboxwidth = Globals.ConsoleWidth - (Globals.ConsoleWidth / 3);
            DrawEnemyBox();
            DrawEnemyImage();
            DrawPlayersBox();
            statusPosition.y = inputPosition.y = imageheight + enemystatboxheight + playerstatboxheight;
        }

        public override void Menu()
        {
            menucommands = "Select option ('exit' to Exit):\n";

            switch (CurrentState)
            {
                case State.Main:
                    Menu_Main();
                    break;
                case State.UseItem:
                    Menu_UseItem();
                    break;
            }
        }

        public override void Menu_Main()
        {
            menucommands += string.Format("Fighting {0}!\n1 - Attack\n2 - Defend\n3 - Use Item\n4 - Run Away", Enemy.Name);
            MenuInput();
            switch (input)
            {
                case "1":
                    Attack();
                    break;
                case "2":
                    Defend();
                    break;
                case "3":
                    CurrentState = State.UseItem;
                    break;
                case "4":
                    RunAway();
                    break;
                default:
                    status = string.Format("Didn't understand '{0}'", input);
                    break;
            }
            if(Enemy.Dead)
                KilledEnemy();
            if(Player.Dead)
            {
                Draw.PromptBox(10, 8, 30, 2, "You died!");
                ExitScreen();
            }
        }

        private void Menu_UseItem()
        {
            itemList.Clear();
            int i;
            for (i = 0; i < Player.Inventory.Items.Count; i++)
            {
                if (Player.Inventory.Items[i].Item is Consumable)
                {
                    menucommands += string.Format("{0} - {1}({2}){3}", i + 1,
                        Player.Inventory.Items[i].Item.Name,
                        Player.Inventory.Items[i].Amount,
                        (i == Player.Inventory.Items.Count) ? "" : "\n"
                    );
                    itemList.Add(Player.Inventory.Items[i].Item.Name);
                }
            }
            menucommands += (i + 1) + " - Go back";
            MenuInput();
            int menunumber = 0;
            if (!int.TryParse(input, out menunumber) || menunumber > i + 1)
            {
                status = string.Format("Didn't understand '{0}'", input);
                return;
            }     
            // Go Back
            if (menunumber == i + 1)
            {
                CurrentState = State.Main;
                return;
            }
            // Menu Item
            InventorySlot invSlot = Player.Inventory.Items.Find(x => x.Item.Name == itemList[menunumber - 1]);
            Consumable item = (Consumable)invSlot.Item;
            status = item.Use(Player);
            Player.Inventory.Remove(item, 1);
        }

        private void Attack(bool playerAttacking = true)
        {
            Random random = new Random();
            int enemyAtk = 0, playerAtk = 0;   // Player and enemy's projected attack values
            bool enemyDefended = false;

            // ENEMY CHOICE - Check what the Enemy will do (10% chance to defend)
            if (random.NextDouble() <= 0.9f) // ENEMY ATTACKS
            {
                // Check Hit Chance for Enemy
                if (random.NextDouble() <= BattleHelper.HitChance(Enemy, Player))   // Check Hit Chance for Enemy
                    enemyAtk = Enemy.Power;
            }
            else // ENEMY DEFENDS
            {
                enemyDefended = true;
                if (random.NextDouble() <= BattleHelper.DodgeChance(Enemy, Player)) // Check Dodge Chance for Enemy
                    playerAtk = -1; // Enemy dodged, uh oh
            }
            if (playerAttacking)
            {
                // Check Hit Chance for Player
                if (random.NextDouble() <= BattleHelper.HitChance(Player, Enemy))
                    playerAtk = (playerAtk < 0) ? 0 : Player.Power; // Check if Enemy has dodged attack

                if (playerAtk > 0 && enemyDefended)   // Player landed an attack and enemy defended
                    playerAtk = BattleHelper.DefendAttack(playerAtk, Enemy.Defense);
            }

            // SET STATUS
            status = (playerAttacking) ? "You attacked!" : "";
            status += (enemyDefended) ? "\nEnemy defended!" : "\nEnemy attacked!";

            if(playerAttacking)
                status += (playerAtk > 0) ? string.Format("\nYou attacked for {0} damage!", playerAtk) : "\nYour attack missed!";
            status += (enemyAtk > 0) ? string.Format("\nEnemy attacked for {0} damage!", enemyAtk) : "\nEnemy's attack missed!";

            Player.Health -= enemyAtk;
            Enemy.Health -= playerAtk;
        }

        private void Defend()
        {
            Random random = new Random();
            int enemyAtk = 0;   // Enemy's projected attack value
            bool enemyDefended = false;

            // ENEMY CHOICE - Check what the Enemy will do (10% chance to defend)
            if (random.NextDouble() <= 0.9f) // ENEMY ATTACKS
            {
                if (random.NextDouble() <= BattleHelper.HitChance(Enemy, Player))      // Check Hit Chance for Enemy
                    enemyAtk = Enemy.Power;
            }
            else // ENEMY DEFENDS
                enemyDefended = true;

            // PLAYER
            if (random.NextDouble() <= BattleHelper.DodgeChance(Player, Enemy))        // Check Dodge Chance for Player
                enemyAtk = 0;

            if(enemyAtk > 0)    // Enemy landed an attack
                enemyAtk = BattleHelper.DefendAttack(enemyAtk, Player.Defense);

            // SET STATUS
            status = "You defended!";

            if (enemyDefended)
                status += "\nEnemy also defended!";
            else
                status += (enemyAtk > 0) ? string.Format("\nEnemy attacked for {0} damage!", enemyAtk) : "\nEnemy's attack missed!";

            Player.Health -= enemyAtk;
        }

        private void RunAway()
        {
            Random random = new Random();
            if (random.NextDouble() >= BattleHelper.DodgeChance(Player, Enemy))        // Check Run Chance for Player
            {
                // Failed, can't run this time.
                status = "You failed to run!";
                Attack(false);  // Have Enemy attempt to attack
                return;
            }

        }

        private void DrawEnemyImage()
        {
            ASCIIImage image = ASCII.Get(Enemy.Image);
            Coord imagePos = new Coord((Globals.ConsoleWidth / 2) - (image.Width / 2), enemystatboxheight);
            imageheight = Draw.TextBox(imagePos.x, imagePos.y, image.Width + 2, image.Height, image.Display, 0, true, Draw.Alignment.Left);
        }

        private void DrawPlayersBox()
        {
            Coord boxPos = new Coord(0, enemystatboxheight + imageheight);
            string text = string.Format("{0}[{1}] {2} {3}/{4}HP",
                Player.Name, Player.Level, StringTools.GetProgressBar(Player.Health, Player.MaxHealth, 15),
                Player.Health, Player.MaxHealth);
            playerstatboxheight = Draw.TextBox(boxPos.x, boxPos.y, statboxwidth, 3, text);
        }

        private void DrawEnemyBox()
        {
            Coord boxPos = new Coord(Globals.ConsoleWidth - statboxwidth, 0);
            string text = string.Format("{0}[{1}] {2} {3}/{4}HP",
                Enemy.Name, Enemy.Level, StringTools.GetProgressBar(Enemy.Health, Enemy.MaxHealth, 15),
                Enemy.Health, Enemy.MaxHealth);
            enemystatboxheight = Draw.TextBox(boxPos.x, boxPos.y, statboxwidth, 3, text);
        }

        private void KilledEnemy()
        {
            Draw.PromptBox(10, 8, 30, 2,
                string.Format("{0} died!\n+{1} XP!", Enemy.Name, Enemy.XPReward)
                );
            Player.AddXP(Enemy.XPReward);
            ExitScreen();
        }
    }
}
