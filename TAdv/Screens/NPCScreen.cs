﻿using Charpix;
using Charpix.ASCIITools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.Drawing;
using TAdv.Statistics;
using TAdv.WorldElements;

namespace TAdv.Screens
{
    class NPCScreen : MenuScreen
    {
        PlayableCharacter Player;
        NPC Npc;

        int column1width = 35,
                column2width = 0,
                toptitleheight = 0,
                imageheight = 0, imagewidth = 0,
                textheight;

        public NPCScreen(PlayableCharacter player, NPC npc)
        {
            this.Player = player;
            this.Npc = npc;
        }

        public override void MainLoop()
        {
            column2width = Globals.ConsoleWidth - column1width;

            DrawTitle();
            DrawNPCImage();
            DrawSpeech();

            statusPosition.y =
            inputPosition.y = toptitleheight
            + Math.Max(imageheight, textheight);
        }

        public override void Menu_Main()
        {
            if (Npc.HasResponses)   // Print out responses if the Dialogue strand permits it
            {
                string[] responses = Npc.Dialogue.GetResponses();
                for (int i = 0; i < responses.Length; i++)
                    menucommands += string.Format("{0} - {1}\n", (i + 1).ToString(), responses[i]);
            }
            else
                menucommands = "Press Enter to Continue...";

            MenuInput();    // Draw input

            int response = -1;
            int.TryParse(input, out response);  // Parse input and see if it's a number
            if (response > 0)
                Npc.Dialogue.Respond(response); // Respond if number
            else
                Npc.Dialogue.Continue();    // Just continue, if not
        }

        void DrawTitle()
        {
            toptitleheight = Draw.TextBox(0, 0, Globals.ConsoleWidth, 1, string.Format("{0} - {1}", Npc.Name, Npc.Description), 0);
        }

        void DrawNPCImage()
        {
            ASCIIImage Image = ASCII.Get(Npc.Image);
            imagewidth = (Image != null) ? Image.Width : 20;
            Coord imagePos = new Coord(0, toptitleheight);
            if(Image != null)
                imageheight = Draw.TextBox(imagePos.x, imagePos.y, Image.Width + 2, Image.Height, Image.Display, 0, true, Draw.Alignment.Left, new ColorInfo(), new ColorInfo(ConsoleColor.DarkCyan));
            else
                imageheight = Draw.TextBox(imagePos.x, imagePos.y, 20, 20, "N/A", 0, true, Draw.Alignment.Left, new ColorInfo(), new ColorInfo(ConsoleColor.DarkCyan));
        }

        void DrawSpeech()
        {
            Coord textPos = new Coord(imagewidth + 2, toptitleheight);
            textheight = Draw.TextBox(textPos.x, textPos.y, Globals.ConsoleWidth - (imagewidth + 2), imageheight, Npc.Talk(), 1, true, Draw.Alignment.Center, new ColorInfo(ConsoleColor.White));
        }
    }
}
