﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TAdv.WorldElements;

namespace TAdv.Physics
{
    [DataContract]
    public class SurroundingObjects
    {
        [DataMember]
        public List<DirectionObject> Objects = new List<DirectionObject>();

        public SurroundingObjects()
        {
            foreach (WorldSpace.Direction direction in Enum.GetValues(typeof(WorldSpace.Direction)))
                this.Objects.Add(new DirectionObject(direction, new WorldObject()));
        }

        public DirectionObject Get(WorldSpace.Direction direction)
        {
            return Objects[(int)direction];
        }

        public WorldObject FindObject(string name)
        {
            name = name.ToLower();
            foreach(DirectionObject direction in Objects)
            {
                if (direction.Object != null && direction.Object.Name.ToLower() == name)
                    return direction.Object;
            }
            return null;
        }

        public DirectionObject Find(string name)
        {
            name = name.ToLower();
            foreach (DirectionObject direction in Objects)
            {
                if (direction.Object != null && direction.Object.Name.ToLower() == name)
                    return direction;
            }
            return null;
        }
    }
    [DataContract]
    public class DirectionObject
    {
        [DataMember]
        public WorldSpace.Direction Direction;
        [DataMember]
        public WorldObject Object;

        public DirectionObject(WorldSpace.Direction direction, WorldObject obj)
        {
            Direction = direction;
            Object = obj;
        }
    }
}
