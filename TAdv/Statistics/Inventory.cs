﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TAdv.WorldElements;

namespace TAdv.Statistics
{
    [DataContract]
    public class Inventory
    {
        [DataMember]
        public List<InventorySlot> Items = new List<InventorySlot>();   // List of items and quantities

        /// <summary>
        /// Add an item to the Inventory
        /// </summary>
        /// <param name="item">Item to add</param>
        /// <param name="amount">Amount of items to add</param>
        public void Add(Item item, int amount = 1)
        {
            if (amount <= 0) // Check if amount to add is 0 or smaller
                return;

            int currentAmount = GetAmount(item); // Get current amount
            if (currentAmount > 0)
                Get(item).Amount += amount; // Add to current amount
            else
                Items.Add(new InventorySlot(item, amount)); // Add a new entry
        }
        /// <summary>
        /// Remove an amount of Item from the inventory
        /// </summary>
        /// <param name="item">Item to remove</param>
        /// <param name="amount">Amount to remove</param>
        public void Remove(Item item, int amount = 1)
        {
            if (amount <= 0) // Check if amount to remove is 0 or smaller
                return;

            int newAmount = GetAmount(item) - amount; // Calculate new amount
            if (newAmount > 0)
                Get(item).Amount = newAmount; // Change to new amount
            else
                Items.Remove(Get(item)); // Remove entry from Inventory
        }
        /// <summary>
        /// Get the InventorySlot specific to the Item
        /// </summary>
        /// <param name="item">Item to search for</param>
        /// <returns></returns>
        public InventorySlot Get(Item item)
        {
            return Items.Find(x => x.Item == item);
        }
        /// <summary>
        /// Get amount of Items in Inventory (if any)
        /// </summary>
        /// <param name="item">Item to search for</param>
        public int GetAmount(Item item)
        {
            InventorySlot found = Get(item);
            if (found != null)
                return found.Amount;
            return 0;
        }
    }
    public class InventorySlot
    {
        public Item Item;
        public int Amount;

        public InventorySlot() { }
        public InventorySlot(Item i, int a)
        {
            Item = i;
            Amount = a;
        }
    }
}
