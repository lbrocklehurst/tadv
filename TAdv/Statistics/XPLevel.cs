﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAdv.Statistics
{
    [DataContract]
    public class XPLevel
    {
        [DataMember]
        private float m_basexp = 100, m_levelmultiplier = 1.5f;
        [DataMember]
        public int XP, Level;

        public XPLevel(int level = 1, int xp = 0)
        {
            Level = level;
            XP = xp;
        }

        public int MaxXP
        {
            get { return (int)m_basexp * (int)Math.Round(Math.Pow(Level + 1, m_levelmultiplier)); }
        }

        public void AddXP(int amount)
        {
            if (amount < 0)
                return;

            int newAmount = XP + amount;
            if(newAmount > MaxXP)
            {
                int leftover = newAmount - MaxXP;
                Level++;
                while(leftover > MaxXP) // Account for multiple levelling up
                {
                    leftover = leftover - MaxXP;
                    Level++;
                }
                XP = leftover;
            }
            else
                XP = newAmount;
        }
    }
}
