﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAdv.Statistics
{
    [DataContract]
    public class AttributePoints
    {
        [DataMember]
        public List<AttributePoint> Attributes = new List<AttributePoint>();
        [DataMember]
        public int SparePoints = 0;

        public AttributePoints()
        {
            foreach (AttributePoint.Attributes attribute in Enum.GetValues(typeof(AttributePoint.Attributes)))
                this.Attributes.Add(new AttributePoint(attribute, 0));
        }

        public bool AssignPoints(AttributePoint.Attributes attribute, int amount)
        {
            if (amount > SparePoints)
                return false;
            if (!Get(attribute).AddPoints(amount))
                return false;
            SparePoints -= amount;
            return true;
        }

        public AttributePoint Get(AttributePoint.Attributes attribute)
        {
            return Attributes.Find(x => x.Attribute == attribute);
        }
    }
    [DataContract]
    public class AttributePoint
    {
        public enum Attributes
        {
            None,
            Strength,
            Dexterity,
            Endurance,
            Intelligence,
            Charisma,
            Speed
        }
        [DataMember]
        public Attributes Attribute;
        [DataMember]
        public int Points, Max;

        public AttributePoint()
        {
            this.Attribute = Attributes.None;
            this.Points = 0;
            this.Max = 0;
        }
        public AttributePoint(Attributes attribute, int point, int max = 20)
        {
            this.Attribute = attribute;
            this.Points = point;
            this.Max = max;
        }

        public bool AddPoints(int amount)
        {
            int newValue = Points + amount;
            if (newValue < 0 || newValue > Max)
                return false;   // Cannot - or + this amount of points
            Points = newValue;
            return true;        // Success!
        }
    }
}
