﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TAdv.Statistics
{
    class Globals
    {
        public const int SaveSlotNumber = 10;
        public static int ConsoleWidth = 70, ConsoleHeight = 50;
        public const string SaveFiletype = ".sav";
        public static string AppDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
    }
}
