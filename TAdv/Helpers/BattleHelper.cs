﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.Statistics;
using TAdv.WorldElements;

namespace TAdv
{
    class BattleHelper
    {
        /// <summary>
        /// Calculate attack damage after a successful defend
        /// </summary>
        /// <param name="attackDmg">Damage defending against</param>
        /// <param name="defenseValue">Defense stat</param>
        public static int DefendAttack(int attackDmg, int defenseValue)
        {
            return attackDmg - (defenseValue / 2);
        }

        /// <summary>
        /// Calculate the Player's hit chance between player and enemy
        /// </summary>
        public static float HitChance(PlayableCharacter player, Enemy enemy)
        {
            return CalculateHitChance(
                player.Attributes.Get(AttributePoint.Attributes.Dexterity).Points,
                enemy.Attributes.Get(AttributePoint.Attributes.Dexterity).Points
                );
        }
        /// <summary>
        /// Calculate the Enemy's hit chance between player and enemy
        /// </summary>
        public static float HitChance(Enemy enemy, PlayableCharacter player)
        {
            return CalculateHitChance(
                enemy.Attributes.Get(AttributePoint.Attributes.Dexterity).Points,
                player.Attributes.Get(AttributePoint.Attributes.Dexterity).Points
                );
        }
        /// <summary>
        /// Calculate the hit chance between opposing Dexterity values
        /// </summary>
        private static float CalculateHitChance(int thisDex, int enemyDex)
        {
            // Make sure values are above 0
            if (thisDex <= 0)
                thisDex = 1;
            if (enemyDex <= 0)
                enemyDex = 1;
            return (float)Math.Pow(0.8f, ((float)thisDex / (float)enemyDex));
        }

        /// <summary>
        /// Calculate the Player's dodge chance between player and enemy
        /// </summary>
        public static float DodgeChance(PlayableCharacter player, Enemy enemy)
        {
            return CalculateDodgeChance(
                player.Attributes.Get(AttributePoint.Attributes.Dexterity).Points,
                player.Attributes.Get(AttributePoint.Attributes.Speed).Points,
                enemy.Attributes.Get(AttributePoint.Attributes.Dexterity).Points,
                enemy.Attributes.Get(AttributePoint.Attributes.Speed).Points
                );
        }
        /// <summary>
        /// Calculate the Enemy's dodge chance between player and enemy
        /// </summary>
        public static float DodgeChance(Enemy enemy, PlayableCharacter player)
        {
            return CalculateDodgeChance(
                enemy.Attributes.Get(AttributePoint.Attributes.Dexterity).Points,
                enemy.Attributes.Get(AttributePoint.Attributes.Speed).Points,
                player.Attributes.Get(AttributePoint.Attributes.Dexterity).Points,
                player.Attributes.Get(AttributePoint.Attributes.Speed).Points
                );
        }
        /// <summary>
        /// Calculate the dodge chance between opposing stat values
        /// </summary>
        private static float CalculateDodgeChance(int thisDex, int thisSpeed, int enemyDex, int enemySpeed)
        {
            // Make sure values are above 0
            if (thisDex <= 0)
                thisDex = 1;
            if (thisSpeed <= 0)
                thisSpeed = 1;
            if (enemyDex <= 0)
                enemyDex = 1;
            if (enemySpeed <= 0)
                enemySpeed = 1;
            return (float)Math.Pow(0.3f,
                ((float)enemyDex + (float)enemySpeed) / ((float)thisDex + (float)thisSpeed)
                );
        }
    }
}
