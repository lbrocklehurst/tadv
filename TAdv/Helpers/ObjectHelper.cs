﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.WorldElements;

namespace TAdv
{
    class WorldObjectHelper
    {
        public static bool Compare(WorldObject object1, WorldObject object2)
        {
            if (object1 == null || object2 == null)
                return false;
            return object1.Name.Equals(object2.Name);
        }
    }
}
