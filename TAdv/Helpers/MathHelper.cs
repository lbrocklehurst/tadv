﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.Statistics;
using TAdv.WorldElements;

namespace TAdv
{
    class MathHelper
    {
        /// <summary>
        /// Clamp a value between min and max
        /// </summary>
        public static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
    }
}
