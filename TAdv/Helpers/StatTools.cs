﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAdv.Statistics;
using TAdv.WorldElements;

namespace TAdv
{
    class StatTools
    {
        /// <summary>
        /// Roll for a value between 1 and 20
        /// </summary>
        public static int RollD20()
        {
            Random random = new Random();
            return random.Next(1, 20);
        }
        /// <summary>
        /// Generate a modifier value (between -5 and 5)
        /// </summary>
        public static int GenerateModifier() { return RollD20() / 2 - 5; }
    }
}
